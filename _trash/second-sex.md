---
layout: post
title:  "The Blurry Line between Sexism and Narcissism"
author: jazzyb
---

> In short, the narcissist is the main character in his own movie.  Not
> necessarily the best, or strongest, but the main character.  [...]  The
> worst thing that could happen to a narcissist is that his wife cheats on him
> *secretly and never tells him, and she doesn't act any differently towards
> him, so that he couldn't even tell*.  If she can do all that, that means she
> exists independently of him.  He is not the main character in the movie.
> She has her own movie and he's not even in it.
> 
> -- The Last Psychiatrist, ["This is Not a Narcissistic Injury"](http://thelastpsychiatrist.com/2006/12/this_is_not_a_narcissistic_inj.html)

### Henry de Montherlant

Women are treated as less than human.  His female characters lack good
reasoning.  They are caricatured as domestic animals.  They are traps for men.
"[T]hey have nothing to give man and can only harm him" (p. 214).

> [I]t is easy to believe oneself sovereign when alone, to believe oneself
> strong when carefully refusing to bear any burden. (p. 216)

> Like them, he has always been a nihilist, he has always hated humanity.
> "People aren't even worth being led (and humanity does not have to have done
> something to you [for you] to detest it to this extent)"; like them, he
> thinks that certain beings -- race, nation, or he, Montherlant, himself --
> are in possession of an absolute privilege that grants them full rights over
> others. (p. 224)

> It is noteworthy that *none* of Montherlant's works depicts a conflict
> between man and man; coexistence is the great living drama: he eludes it.
> His hero always rises up alone facing animals, children, women, landscapes;
> he is prey to his own desires (like the queen of *Pasiphaé*) or his own
> demands (like the master of Santiago), but *no person* is ever beside him.
> Even Alban in *The Dream* does not have a friend: when Prinet was alive, he
> disdained him; he only exalts him over his dead body.  Montherlant's works,
> like his life, recognize only *one* consciousness. (p. 224-5)

### D. H. Lawrence

Man and woman both have their place.  The woman is the polar opposite and has
a different place in life than the man.  One is assured in his vitality, the
other assured in her femininity.  Neither show much subjectivity.  His
concepts of sexual union and marriage are quite phallic in characterization.
Woman is to submit her will to man; man is to acknowledge his need for woman.

He doesn't come across as a narcissist to me like Montherlant does.

### Paul Claudel

TODO

### Real World Example:  The New Sexist Male Fantasy

Liana Kerzner, ["Are Daughters The New Video Game Male Fantasy?"](https://www.youtube.com/watch?v=6KYKxN2gZkU)

Also worth mentioning the same problem being the core narrative of
[Pitch](https://www.youtube.com/watch?v=XJIXK24lqU8).

The problem is sometimes framed as a sexist male fantasy:  The daughter is
nothing but an extension of the father's desires or goals.  There is some
truth to this in Liana K's example of the video game industry.  Men instead of
women are being asked to come up with a narrative for a game with a female
protagonist.  The men have difficulty telling the story from the perspective
of a female character, but they *can* tell the story from the perspective of a
male character -- the father; the daughter then becomes the proxy for the
father.

To criticize the narrative as a sexist male fantasy misses the point.  The
problem isn't that a daughter is mere proxy for the father; the problem is
that a *child* is proxy for the *parent*.  Change the gender of either party,
and the problem remains the same.  Thus, it is not sexist but rather
narcissistic.  The narcissist has already lost his opportunity to achieve his
dreams.  He was supposed to be a rockstar or a professional athlete.  For
years he might have convinced himself that he still could.  Yet as the years
pass by, he becomes less able to tell himself that he will ever be that sort
of main character in the movie.  But his child has time to fulfill his
ambitions.  The child may achieve the dream, but the dream is the parent's.
The child is an object, not a subject, not independent of the parent's movie.
The parent thereby remains the main character.

Without doing an exhaustive check, it seems like the form of the fantasy in
popular culture is clearly in favor of father-daughter relationships as
opposed to any other gender of parent-child relationship (like, say,
father-son).  I wonder if this is due to an attempt to create female-centered
storylines, that it -- like the game above -- is an attempt at feminist
storytelling but without a realization on the part of the author that they are
still making a main character that is their proxy (the father).

The real problem is not that this is a fantasy.  If this problem were limited
to fictional narratives, the worst we could say about it is that it
demonstrated bad aesthetics.  The problem is that this "fantasy" plays out in
real life all around us.  Fathers who failed as professional athletes push
their sons to excel in sports that they may have no passion for themselves;
parents who are disappointed in one way or another with their lives seek to
live vicariously through their children without recognizing their children as
autonomous agents who have independent lives.
