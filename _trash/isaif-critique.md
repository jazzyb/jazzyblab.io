---
layout: post
title:  "A Critique of <i>Industrial Society and Its Future</i>"
author: jazzyb
---

> We are not used to a complicated civilization, we don't know how to behave
> when personal contact and eternal authority have disappeared.  There are no
> precedents to guide us, no wisdom that wasn't made for a simpler age.  We
> have changed our environment more quickly than we know how to change
> ourselves.
> 
> -- Walter Lippmann, <u>Drift and Mastery</u>

<br>

### Introduction

Between 1978 and 1995 Theodore J. Kaczynski delivered 16 bombs to people whom
he deemed were advancing the progress of "modern industrial society" -- mostly
professors, corporate executives, and technologists.  The FBI dubbed him the
"Unabomber" after his initial targets:  **UN**iversities and **A**irlines.  His
terrorism killed three, injured twenty-three, and ended only when The New York
Times and The Washington Post agreed to publish his 35,000-word manifesto,
"Industrial Society and Its Future" (ISAIF).

In his manifesto Kaczynski argues that the path of technological progress will
eventually lead to either the destruction or enslavement of the human race.
An international revolution against "the system" to destroy all modern
technology and throw the world back into pre-industrial society is the only
way to save humanity.  Kaczynski justifies his violent means to popularizing
these views:

> To make an impression on society with words therefore is almost impossible
> for most individuals and small groups. [...] In order to get our message
> before the public with some chance of making a lasting impression, we've had
> to kill people.[^1]

[^1]:  Theodore J. Kaczynski, "Industrial Society and Its Future"

What Kaczynski did not anticipate was that his terrorism, which led to the
manifesto getting published, would ultimately prevent the majority of the
public from viewing the work as anything but mad ravings.  Various writers
have attempted to critique the manifesto but seem to stop short of addressing
the heart of his work, instead focusing on his obvious mental instability and
dearth of original ideas.  While it is true that Kaczynski is mentally ill and
many of his ideas have already been expressed in some form by better writers,
I believe the concerns he raises are real and worth serious examination --
hence, this attempt at my own critique.

However, this post will not be a complete critique of the manifesto.  My
impression of the work is that it contains three different themes:  (1) a
criticism of the ills of modern society, (2) an examination of the philosophy
and attitudes of what he terms "leftism", and (3) how one should organize the
revolution to overthrow industrial society.  This post will focus only on (1).
While I think what Kaczynski has to say about "leftism" is interesting, I
think it is, for the most part, ancillary to his criticism of modern society.
Thus, I will skip it for now but may address those ideas in a later post.  I
will also ignore Kaczynski's ideas about revolution because they rely on the
truth of (1), and as I will expand upon below, I believe his criticism does
not justify the sort of revolutionary action he advocates.

(A quick note on references before I begin:  Kaczynski numbered each paragraph
of his manifesto so I will mark any idea or quote from the manifesto by a
superscript § indicating the paragraph.  For example <sup>§4</sup> indicates
that the information comes from paragraph 4, and <sup>§§55-58</sup> indicate
the information comes from paragraphs 55 through 58.  The reader should assume
any quotes in this post come from "Industrial Society and Its Future" unless
otherwise specified.)

### The Power Process, Surrogate Activities, and Freedom

We must begin our understanding of Kaczynski's manifesto with his theory of
how humans are driven to find fulfillment in their lives.  Many of his later
criticisms of technology and industrial society are directly related to this
theory which he calls the 'power process'.<sup>§39</sup>  The 'power process'
is composed of four parts:

1. People have goals and drives that they need to achieve to give their lives
   meaning.
2. Achieving these goals must require effort.
3. People must undertake this effort *autonomously*, i.e. on their own
   initiative and under their direct control.<sup>§42</sup>
4. People must regularly attain their goals.

Kaczynski distinguishes between three kinds of goals:  trivial goals that
require little to no effort to achieve, goals that require serious effort, and
goals that are impossible or nearly so to achieve.<sup>§59</sup>  The only
goals capable of providing true fulfillment within the 'power process' are
those of the second sort.  If one attains one's goals too often, then the
result is boredom; if too seldom, then the result is frustration or anxiety.
In humanity's pre-industrial past goals directly related to survival --
finding food, water, shelter, etc. -- tended to fall into the second category,
and in Kaczynski's estimation people are naturally wired to be fulfilled by
the achievement of such drives.  However, in western industrial societies more
and more of our survival drives fall into the trivial category so that there
are fewer goals left that bring true fulfillment to the individual.

When survival becomes trivial to attain, people must find or create artificial
goals and drives that require serious effort to achieve.  Kaczynski calls any
such artificial goal a 'surrogate activity'.<sup>§§38-40</sup>  More
specifically, if an individual would not engage in an activity if he were
required to devote serious effort to providing for his physical necessities,
then that activity is a 'surrogate activity'.  Kaczynski reckons that the
pursuit of, e.g., serious stamp-collecting or grandmaster-level skill in chess
or deep knowledge in a scientific sub-field is a 'surrogate activity', but the
sex drive is not a 'surrogate activity' because people would seek out sex even
if they had to devote serious effort to survival.  Kaczynski considers
'surrogate activities' "demeaning"<sup>§86</sup> to the people who engage in
them because the only reason for pursuing them is to fulfill the 'power
process', but the original purpose of the 'power process' is to drive
survival.  He also considers them demeaning because artificial goals generate
less fulfillment than real goals because the seeker of artificial goals is
never satisfied with their achievement<sup>§41</sup>:  The collector is always
looking for the next stamp; the grandmaster is always improving his game; the
scientist no sooner answers one question than he begins questioning another.

I must admit that I don't quite understand exactly what makes an "artificial"
goal less worthy of devotion than a "real" goal.  The achievement of goals
related to one's survival also require constant striving -- the acquisition of
new food, the maintenance of one's shelter, etc.  In fact I see the
progression of survival-related drives from the serious to trivial category as
beneficial.  I believe most pre-industrial people would gladly trade their
day-to-day existential risk for a modern existential crisis.

It's also worth pointing out that the difference between real and artificial
goals seems to be in the degree of effort more than the object of that effort.
For example, while Kaczynski would say that the pursuit of a grandmaster-level
skill in chess is a 'surrogate activity', I doubt he would say that playing
games in general is a 'surrogate activity'.  Game-playing is universal enough
that it is safe to assume that people would play games with one another even
if they spent considerable effort providing for their necessities.  Likewise,
asking questions about who we are and where we come from is not a 'surrogate
activity', but the pursuit of knowledge in a scientific sub-field is just the
modern-day equivalent of that same ancient wondering that was once the realm
of myth and religion.  Engaging in these drives has become more efficient and
thorough, but the drives themselves are every bit as natural as the ones to
survive and reproduce.  And once again, I certainly prefer these modern
concerns over anxiety for physical necessities.

Kaczynski emphasizes the failure of artificial drives to completely fulfill
and satisfy modern man and blames this on industrial society, but a cursory
reading of religious history will reveal this problem to have predated the
Industrial Revolution by at least 2,000 years.  The Buddha spoke about many of
the same problems in the minds of primitive people, so I doubt that bringing
an end to technological progress will usher in a period of greater spiritual
satisfaction.

Even though I criticize some of Kaczynski's conclusions, I do think he is on
to something when he presents the 'power process' as the driver of human
motivation.  I can bring myself to believe that this psychological process
evolved to drive human survival, and now that our physical needs are mostly
cared for, it has been co-opted to drive concerns that would have been
unnecessary in our pre-industrial past.  Nevertheless, I do not think that
this alone is reason to say we should return to a more primitive society.

<!--- TODO: Better transition between above and below paragraphs -->

One more concept is worth exploring before diving into the heart of
Kaczynski's critique of modern society, and that is his concept of 'freedom'.
Kaczynski distinguishes 'freedom' from the modern concept of 'natural rights'
which most would consider synonyms.  Kaczynski defines 'freedom' as:

> the opportunity to go through the power process, with real goals not the
> artificial goals of surrogate activities, and without interference,
> manipulation or supervision from anyone, especially from any large
> organization.<sup>§94</sup>

'Freedom' is not a right which is either allowed or protected by a government.
For Kaczynski, one's 'freedom' is largely determined by how much one is exempt
from retribution for exercising the 'freedom'.  For example, the US
constitution grants citizens the right of freedom of speech so that one is
protected from prosecution for speaking one's mind, but this is a right which
could be legislated away with legal exceptions to speech.  However, another
person may live under the rule of a despot, but if he is not important enough
to come to the attention of the ruler or the ruler lacks the resources to
enforce criticisms of his rule, then the subject has a greater freedom of
speech because it has no practical limits like rights of the US
citizen.<sup>§§94-95</sup>

<!---
### The Modern Industrial-Technological System

TODO:  Talk about the primitive, anarchistic society that Kaczynski thinks is
the ideal and use that to describe the sort of society that he rails against.

Now that we understand something of Kaczynski's view of the human condition,
it's important to understand the environment he believes is damaging that
condition.  Even though the manifesto spends significant amounts of ink on the
negative aspects of "industrial-technological society" (sometimes called
"industrial society", "technological society", or simply "the system"), it
offers little in the way of a straight-forward definition of exactly what this
term refers to -- at least little more than the vague concept of "the modern
world".  Nevertheless, Kaczynski provides enough clues in his arguments that
we should be able to piece together some picture of what he means when he
talks about 'the system'.

Kaczynski's chief criticisms of 'the system' derive from two sources:  that it
separates man from Nature and that it denies man autonomy.  The first
criticism is not quite the naturalistic fallacy that it at first seems.
Nature is important for us because it is the environment our ancestors evolved
to suit[^2].  Therefore, our physiology and psychology are adapted best for
that pre-industrial environment.  The further we leave our ancestral home, the
less likely our new society will suit our physical and mental health.  For
example, the 'power process' evolved to help us survive, but the more our
physical needs are trivially met, the more likely we are to become bored or
anxious.  Likewise, we are not required to use our bodies in the same way we
did in our evolutionary past, and we must find time to keep our bodies healthy
in ways that primitive man never had to consider.<sup>§75</sup>  The
technological aspect to 'the system', at least since the advent of the
industrial revolution, has "progressed" so quickly, and continues to
exponentially, that we have no hope of naturally adapting to the new
society.<sup>§49</sup>

[^2]:  Or, alternatively, God designed both us and Nature to suit one another.
       Throughout ISAIF Kaczynski is ambivalent about which explanation he
       supports because it doesn't really change any of his arguments.  I will
       stick with the evolutionary explanation throughout this post for the
       sake of simplicity.

The second source of criticism -- that 'the system' denies man autonomy -- is
directly linked to how technologically advanced we have become.  As technology
has advanced through first the train, telegraph, telephone, plane, and finally
internet, humanity has become more interconnected.  This has caused a shift in
focus from the local, or the individual, to the global.  Each individual must
do their part for 'the system', so drives are imposed from without.  This is
not to say that primitive society could not demonstrate the same lack of
autonomy, but Kaczynski believes that the efficiency with which technological
society operates today has made the problem inescapable.<sup>§58</sup>

-->

### The Psychological Effects of the System

As I explained above, many of the problems which Kaczynski has with the system
are due to modern society's inability to fulfill the power process
autonomously.  Our autonomy under the system is not just threatened by
governments and rules and regulations; each person is subjected to indirect
manipulation by advertising and propaganda.  We are manipulated into wanting
things so foreign to any real desires that we wouldn't even know how to miss
them in a primitive society.  Additionally, society imposes certain status
quos on us.  For example, nothing prevents any of us from going into the woods
and living off the land, but there is very little wilderness left, especially
to support the population we have.  Thus, even though we *practically* have
many options in society, the system is organized in such a way that to escape
it is difficult.

### Technological Society's "Progress"

---

Outline:

<!---
* Power process<sup>§39</sup>
  1. everyone needs goals
  2. achieving goals must require effort
  3. effort must be autonomous<sup>§44</sup>
  4. goals must be regularly attained
* Surrogate activities
  * activities that a person would not pursue if the person was spending most
    of their time acquiring physical necessities<sup>§39</sup>
  * the only method in modern society to approximate the power process<sup>§§59-60</sup>
  * these are "demeaning"<sup>§86</sup>
* Freedom
  * "the opportunity to go through the power process, with real goals not the
    artificial goals of surrogate activities, and without interference,
    manipulation or supervision from anyone, especially from any large
    organization"<sup>§94</sup>
  * constitutionally guaranteed rights are less important to individual
    freedom than being able to evade the attention of those in
    charge<sup>§95</sup>
-->
* Modern society causes psychological problems
  * some related to insufficient power process<sup>§§59-60</sup>
  * true autonomy is difficult to achieve<sup>§73</sup>
  * lack of security and power over one's fate<sup>§§67-69,197-199</sup>
  * things change too quickly<sup>§49</sup>
  * some people don't experience problems in modern society; they are
    either:<sup>§§77-85</sup>
    1. content in their servitude
    2. derive so much fulfillment from their surrogate activities that they
       feel no need to achieve "real" goals
    3. achieve goals vicariously through their membership in a powerful
       organization
* Progress of technology and industrial society
  * the progress of society is mostly arbitrary<sup>§106</sup>
  * control of human behavior is the end goal of the
    system<sup>§§143-155</sup>
  * "enthusiasm for 'progress' is a phenomenon peculiar to the modern form of
    society, and it seems not to have existed prior to the 17th
    century"<sup>§210</sup>
  * the good of the system cannot be separated from the bad<sup>§122</sup>
  * freedom may only be exercised in the service of the system, not the
    individual<sup>§97</sup>
  * the system must regulate behavior to continue<sup>§§114,119-120,123-124</sup>
  * local autonomy, power, and rights are sacrificed to the
    global<sup>§§52, 117-118</sup>
  * technological progress is often introduced as optional, but it changes
    society such that people will eventually be forced to use
    it<sup>§§125-130,158-160,170</sup>

---

Pre-reqs:
* Ulrich Beck, Risk Society
* Anthony Gibbons, Runaway World
* Jacques Ellul, The Technological Society

---

What exactly is the "industrial-technological system"?

What prevents the system from being modified; why must it be destroyed?

(§5) Note that some effects of the system that are widely recognized and
talked about have been left out of the manifesto.  This probably includes
positive things about the system so don't bother criticize FC for leaving it
out.

I think I will not discuss his views of "leftism".  These make up a good bit
of the manifesto, but the psychology of leftism is not necessary for
understanding FC's justifications for revolution.  He only seems to mention
them to warn of their effect on any potential revolutionary action (see the
Ship of Fools short story).

(§29) The "oversocialization" of leftists is mentioned as a support of the
system -- making productive members of society out of minorities.  Might be
worth mentioning.  Particularly since it echoes another point of his elsewhere
that it is appropriate to rebel against the system as long as the rebellion is
*unimportant* -- e.g. in one's clothing or music (§72).

(§32) "The problems of the leftist are indicative of the problems of our
society as a whole."

(§33) The "power process":
	* everyone needs to have goals
	* the person must expend effort to try to achieve a goal
	* that effort must be autonomous -- it must have been decided upon by
	  the individual (at least in part), not demanded by someone else
	  (§44)
	* the person must regularly attain goals
		- "Consistent failure to attain goals throughout life results
		  in defeatism, low self-esteem or depression."
		- summary in (§37)

(§39) "Surrogate activities":  any activity directed toward and "artificial
goal".  If a person would have to devote most of their activity to acquiring
physical necessities, and they were engaged in doing so, would they try to
attain a particular goal?  If the answer is 'no', then the activity is a
surrogate.  The study of marine biology, in FC's opinion, is a surrogate
activity; the pursuit of sex is not.

This one of the points that I think FC misses.  The answering of the questions
of "why" and "how" are every bit a part of the human drive as sex.  In that
sense, some studies of science may not be surrogate activities.  Religion and
myth filled this desire before industrial society.  Humanitarian work is also
mentioned as a surrogate activity (§40).  While I agree that it *can* be, it
doesn't have to be since it could be a consequence of our natural empathy.  We
don't like seeing others we sympathize with in pain.  If the majority of our
activity were directed to necessities, we would still feel the need to help
the less fortunate.  (And a technological society can *better* fulfill such
needs.)

(§41) Is this a condition of modern society or of human nature generally?
Because the Buddha talked about this constant striving after goals 2500 years
ago.  Perhaps "the system" is applicable not just to the current technological
society but to any large human system -- like an Indian kingdom of the
6<sup>th</sup> century BC?  FC talks elsewhere about sending society back
500-1000 years.  Surely there is more to it than that if we want to solve the
above problem.

(§45) "Any of the foregoing problems can occur in any society, but in modern
industrial society they are present on a massive scale."  Primitive man was
psychically better off than modern man, but what exactly does he mean by
"primitive man"?  How much technology and ease must he lack to be considered
"primitive" in FC's sense?

(§49) Modern society changes too quickly.  There is no foundation which modern
man can hold on to.

(§51) Interesting connection to the TV effect mentioned in <u>Bowling
Alone</u>.  He talks about the technological system or society as having a
motive.  Where does this drive come from?  By some inherent inertia or force
or via winners of the system who have their own motives for submission to the
system?

(§52) Global concerns begin to supersede local concerns.  Can this be
prevented?

How do we find evidence for his assertions that modern society (specifically
the 20<sup>th</sup> century) is responsible for these psychological problems?
What would we be looking to measure?  This sounds like the data from Bowling
Alone -- in which case the problem is far more recent than FC supposes and is
unlikely caused directly by industrialization.  What aspects of the data
mentioned in Bowling Alone would relate to the power process defined above?

(§§59-60) The power process is the process of satisfying drives which can only
be satisfied at the cost of serious effort.  Trivial and impossible drives are
becoming increasingly common in modern society, and only "artificial drives"
can be satisfied with serious effort.

(§64) Lack of spiritual fulfillment.

Is capitalism a good system at this scale?  Advertising and marketing modify
consumers' desires and keep them ignorant of information about goods.  An
economy like socialism or communism on the other hand would probably be worse
in FC's opinion because the ideal would *better* provide for the necessities
of the people and further affect the natural power process.  (However, it
could also lead to greater autonomy -- see the hunter-fisher-critic passage in
Marx's <u>The German Ideology</u>.)

(§67) Modern man feels that he is not "secure" which leads to a feeling of
powerlessness.  Security has become an impossible drive to achieve.  I wonder
if it is significantly different than the situation that primitive man found
himself in.  Modern society shelters us very well from much of the variability
of the environment -- drought, reliable access to food, extreme heat and
extreme cold, fatal disease, physical violence.  Is this significantly
different from the lack of control modern man experiences with respect to
safety standards and pollution.  Perhaps the consequences of any one thing
failing have a greater affect on more people, but for any one individual I
don't see how they could see themselves as feeling *more* powerless than the
primitive man.

(§68) Good response to the above concerns.

(§69) What difference does it make that one unpreventable threat is caused by
chance and a different one is caused by human agency?  Both are unpreventable
-- shouldn't either be faced "stoically" if they are both equally
unpreventable as FC says.  I would also argue that "chance" is a relatively
modern concept.  Primitive man might have blamed disease on witchcraft or a
spiteful deity.  Agency was still assumed to be behind the act.  Maybe FC
would say that in this case, primitive man did not feel helpless against these
things -- he might burn the witch or make a sacrifice to his god or any other
action which made him feel in control of the situation.  Is this any different
from the democratic process?  Individuals can cast a vote; they can write
their representatives; and through these actions they can feel like they have
some control over the unpreventable threats in their life.

"Elaborating on the conception of Hamlet as an intellectual who cannot make up
his mind, and therefore is a living antithesis to the man of action, Nietzsche
argues that a Dionysian figure possesses knowledge to realize that his actions
cannot change the eternal balance of things, and it disgusts him enough not to
be able to make any act at all." -- from Nietzsche wikipedia page.

I feel like a reading of Nietzsche would be good for understanding some of
FC's perspective.  Nietzsche thought that the act of valuing goals and
striving for those goals made great men regardless of what the goals or values
actually were.  FC, like Nietzsche, thinks that the striving is necessary to
fulfillment (n.b. attainment, too, for FC), but unlike Nietzsche, FC tries to
find an objective set of goals which he grounds in survival.

(§73) We think we are free to do as we please, but much of our activity is
regulated -- by society informally as well as the government formally.  Most
people fit their lives within the system because true autonomy -- living off
the land, starting our own business -- is made more difficult by society.

(§75) We are not required to use our bodies the same way that we did in our
evolutionary past.  Therefore we must find time to keep ourselves healthy in
ways that primitive man never had to think about.  I don't see that this is
necessarily a negative.

Another thought:  He mentions that modern man is afraid of entering successive
stages of life -- youth to fatherhood to old age -- because they have not
adequately experienced the power process.  And yet the evangelical Christian
community in the US does emphasize and esteem such a life journey while also
living in the same society as the existentialist.  Why do they not experience
the same modern anxieties?  Is it their belief in an afterlife?  To me it
indicates a spiritual problem/solution rather than a modern one.

(§76) I don't see how the destruction of the technological system necessarily
gives people autonomy.  I get the sense that his view of pre-industrial
society (or at least the kind he envisions for the future) is a very specific
sort -- like a patchwork of diverse, intentionally small and local anarchistic
societies.  Whereas, pre-industrial society was often made-up of master-slave
societies where some (many?) still lacked autonomy.  Does he just want to do
away with such societies regardless of the technological advancement of the
society?  Could we get the sort of anarchistic hunter-gatherer society he
envisions *with* technology?  Doubtful, since it sounds like it requires fewer
people on the earth and a reduced ability to trade.

(§§77-85) FC describes the sort of people who do not experience psychological
problems in modern society.  They are either (1) content in their servitude,
(2) derive so much fulfillment from their surrogate activities that they feel
no need to achieve "real" goals, or (3) achieve goals vicariously through
their membership in a powerful organization.

(§86) A curious statement:  Even if industrial society had little negative
effect on the population, FC would still be against it because they find
choosing surrogate activities over the pursuit of "real goals" "demeaning".
I'm not entirely certain why he thinks surrogate activities are so demeaning.

(§§87-91) Science as a surrogate activity.  I don't think he understands the
motivation behind scientific curiosity.  This is related to something he said
earlier:  A surrogate activity is something that would not be pursued if a
person needed to devote most of their energies towards survival (§39).  But
the motivation behind scientific curiosity is (I think) understanding the
world around us -- how did we get here, where are we going, what (if anything)
is the meaning behind it all.  Goat-herders did this as they watched the stars
above their flocks 2000 years ago.  The impetus is at least as old as
humanity.  The methods of answering these questions have become more
sophisticated, but the drive behind them is the same.

That most academics engage most of their activity in very narrow sub-fields is
indeed in many cases an artifact of modernity.  Broader, more complex
questions require time and resources to answer that no one academic has time
for between earning a pay-check and living their life.  Thus the focus on
narrower, more well-defined questions to attack, but even the answers to those
questions might serve a larger question in the end.  That modern science is
antithetical to solving larger questions may be a valid critique, but FC does
not address it.

(§92) This is a valid point:  Science (and technology in general) tend to
march forward without regard for the negative effects they will have.  An
evaluation of the ethical repercussions of a line of inquiry are seldom
considered.

(§94) Freedom:  "the opportunity to go through the power process, with real
goals not the artificial goals of surrogate activities, and without
interference, manipulation or supervision from anyone, especially from any
large organization."  So the societies which must be over-thrown are not
limited to those of industrial-technological societies.  Any society that
takes away control of achieving "real goals" related to one's survival is to
be destroyed as well (see §41 and §76) -- definitely has a specific
anarchistic society in mind, not just pre-industrial.

(§95) Constitutionally guaranteed rights are less important than being able to
evade the attention of those in charge.  (Privacy as an inalienable right,
perhaps?)

(§96) Is the right of freedom of the press even applicable in a world where
large corporations monopolize access to information and everything else is
lost in the deluge of "news".

(§97) Beware of "freedoms" that may only be exercised in the service of the
system but not the individual.

(§§99-110) Principles of History.

(§§111-113) Justification via the above principles of revolution instead of
reform.

(§114) The system must regulate human behavior in order to continue.  It
cannot allow unrestrained freedom and hope to keep the gears turning.  This
results in a sense of powerlessness by individuals.

(§115) Education criticism:  The system requires technical specialists so
children are forced to study technical topics which have no impact on their
actual lives.

(§117) Technological society is highly interconnected -- so much so that it
cannot be broken down into independent parts.  This requires great
organization.  A few higher-ups make decisions, but no one individual has any
say in what happens.  Propaganda is used to make people think they want what
has already been decided for them (a la Bernays).  His argument once again
ends with the point that even if people did want this (propagandized or not),
it would be "demeaning" (see §86).

(§118) Due to the interconnectedness and organization of technological
society, local autonomy becomes impossible.

(§§119-120) SO MUCH GOOD CRITIQUE OF TECHNOLOGICAL SOCIETY HERE.
Technological society molds humans to adapt to it rather than humans
developing it to suit the current needs of people.

(§121) The "good" of technological society cannot be separated from the "bad".

(§122) Genetic engineering is inevitable, and it is a bad thing because
humanity will no longer be the product of chance/God/nature but a manufactured
product.  I am uncertain of what makes this necessarily bad.

(§123) "If you think big government interferes too much in your life NOW, just
wait till the government starts regulating the genetic constitution of your
children."

This is a good point which is worth expanding on.  If a person is identified
as on the autism spectrum today, they may or may not go through treatment to
help them navigate society.  What happens when we can identify autism before
the child is born?  If we can modify their genes before most of the "damage is
done" to their brain development in the womb, do we?  Those with autism have
difficulty fitting in with the larger society.  Does it become right to give
them an easier life by modifying their genes to help them fit in?  And if this
does indeed make their lives easier than those who don't have the procedure
done, at what point does society step in and require parents of potentially
autistic children to have the procedure done for the benefit of their
potential children?

https://www.theguardian.com/world/2002/apr/08/davidteather

A lesbian couple intentionally had a deaf child.  Many culturally deaf people
don't view deafness as a disability even though it has disadvantages in our
society.  Should couples be prevented from having deaf children if it can be
prevented?  What disabilities will be permitted, and what disabilities will be
considered equivalent to child abuse?

Is any prevention of disadvantageous phenotypes equivalent to genocide?

(§124) A code of ethics will not work because any such code will result in a
select few imposing their values on the genetic constitution of the population
at large.

(§§125-130) Technology, by its nature, continually encroaches on freedom.  New
technology is often introduced as optional, but it changes society in such a
way that people will eventually be forced to use it.  Technology once
introduced and having changed society cannot be called back; "progress" moves
forward.

(§131) When faced with the choice of getting their job done and doing the
right thing, people tend to sacrifice their ethics to do their job.

(§§132-133) Because technology always progresses, any attempt to limit its
influence short of destroying the whole system will fail when those against it
let down their guard.

**The only problems that the system solves are the problems that threaten the
system.**

(§§140-142) Revolution instead of reform.

(§§143-152) In the past society has reached certain limits when it tried to
control too much of human behavior.  Society is becoming more efficient (via
technology) at controlling human behavior.  In doing so it can push further
beyond the bounds of human nature.  Behaviors are modified or discouraged
which do not fit into the society, but no one is stopping to ask if the
problems are the behaviors or the problems are the burdens placed on us by an
arbitrary society.

The above sections relate back to his point in §122:  Our societal progress is
arbitrary -- not necessarily good or bad -- but when human nature no longer
works within that society, our first instinct is to modify the human behavior
instead of asking if it is the environment that should change to suit
humanity.

(§§153-155) Control over human behavior is introduced in increments that each
taken alone are innocuous, even beneficial, but we fail to think about the
long-term effects on human nature.  Are the changes we are making actually
good, or is it society that should change?

Similar to one of FC's examples, take schooling:  We sometimes resort to
drugging children to stay still in a school setting so they can learn.  Do we
stop to consider if the problem is the school setting, and the child is having
a perfectly natural reaction to an unnatural environment?  That other children
have no problem adapting should not count as evidence to the contrary; perhaps
the solution should be that different children learn in different environments
or at different rates.

(§§158-160) The technological modification of human behavior will only become
more efficient and thorough.  Laws and regulations cannot prevent the advance
because these changes will happen incrementally.

(§161) So far the system is largely unsuccessful at controlling human behavior.

(§162) Within 40-100 years the system will have either taken control of
humans, or humans will have demolished the system.

(§§163-164) Control over everything on Earth, including human behavior, is the
end goal of the system.  Behavior will be brought under control because (1) it
is a sufficiently interesting problem for scientists and (2) for the "good of
humanity".  (One of his concerns is the shift from individual-centered society
to communal society.)

(§§165-166) He predicts a time of intense stress on the system in the next few
decades.  That is the time that we must strike to destroy technological
society for good.

(§167) There will be severe breakdowns in society as a result of the
destruction of technological society.  Most of humanity will die.  It is
probably best this happens sooner rather than later to reduce the damage done.

(§168) "In the second place, one has to balance struggle and death against the
loss of freedom and dignity.  To many of us, freedom and dignity are more
important than a long life or avoidance of physical pain.  Besides we all have
to die sometime, and it may be better to die fighting for survival, or for a
cause, than to live a long but empty and purposeless life."

(§169) The success of the system may ultimately lead to more suffering than
its breakdown would.

(§170) Technology likes to think it will provide solutions to all the problems
that it creates, but it always creates more problems than it solves.
Especially in this section, FC sounds like Karl Marx's critique but without
its faith in history's inevitable march to Communism.  FC is Marx if Marx
lived today and believed immediate revolution was necessary.

(§§171-175) Possibilities for society's future:
1. AGI is invented -- no telling what will happen when machines take over.
2. Improved AI but humans maintain control -- much like our system now, but
   techniques of control will have improved.  Most humans will merely be
   domesticated animals.
3. Human work remains necessary -- humans will still need to be engineered to
   fit into the system, either made docile (as above) or needlessly
   competitive to achieve few positions of real prestige and power.

AI without a sufficient ethical system may simply grow -- even in case #2 --
to be an extension of the system and will have the goals of the system it was
created within as its values and goals.  We would need to have our goals and
values determined to be correct by then.

(§176) OR some combination of the above scenarios.

(§177) Emphasis again on the negativity of being engineered versus
chance/God/etc.  Even those in charge will become slaves to the way the system
runs.

(§181) Two revolutionary goals:  (1) promote social stress and instability to
industrial society and (2) develop and promote an ideology which opposes
technology and the system.

(§§183-184) The ideology must not just be against technology; it must also be
*for* something -- "Nature".  This borders on the naturalistic fallacy.  He
gives several reasons why "Nature" is better than our current system, but none
of them demonstrate that "Nature" is better -- see Yudkowsky's "Evolving to
Extinction".

(§§185-189) Two ideologies must be developed:  one for the elites who
understand the pros and cons of destroying the system and another which can be
marketed to the masses to see the conflict in "unambiguous terms".  Tips on
how to best sell the propaganda.

(§§190-192) The propaganda should be about the masses vs. the power-wielding
elites, NOT the revolutionaries vs. the masses.  The enemy to be vanquished is
the techno-industrial society; anything else is ancillary.

(§§193-194) Not a political revolution -- should be a revolution by outsiders.

(§195) Revolution must be carried out internationally simultaneously.

(§§197-199) Modern man has power OVER nature.  FC regards this as evil.
Pre-industrial society had power WITHIN nature -- e.g. when hungry, they knew
how to hunt and gather.

(§§200-201) Destruction of the system must be the ONLY goal.  Anything else,
while possibly desirable, is a distraction.

(§§204-205) Revolutionaries should have as many children as possible in order
to pass on their goals and values.

(§208) The objective is to destroy technology that is dependent on large-scale
organization but not "small-scale technology", meaning anything that can be
invented/created individually.  However, I doubt this distinction is as
clear-cut as FC wants it to be.  It wouldn't take terribly much ingenuity to
re-invent steam-engines, metal lathes, alcohol-driven internal combustion
engines, etc.  The small-scale technologies which he envisions would (I
suspect) become large-scale once different groups perfected them and began
competing with other groups for resources.

(§210) I still disagree on the time-line, but he makes an interesting point:
"there is no reason anyone would be interested in rebuilding industrial
society.  The enthusiasm for 'progress' is a phenomenon peculiar to the modern
form of society, and it seems not to have existed prior to the 17th century or
thereabouts."  How true is this?  I have been criticizing under the assumption
that progress (defined as just simply wanting to improve one's lot in life)
was human nature and that the drive to improve would naturally (quickly) lead
to technological progress.  But FC might have a point that scientific or
technological "progress" is due to some modern philosophy.

(§212)  "500-1000 years" -- I'll mention one more time.  I think he
underestimates the rate of technological advancement once an idea has been
proven possible.  For example, I have never created a steam engine, but I know
that water can be heated and the extra pressure that the steam creates can be
used to move machinery.  If I set my mind to it, I doubt it would take more
than a couple of years before I could fashion a working, pragmatic steam
engine.  You might say, "But you'll be too busy fulfilling a real power
process -- i.e. trying to survive."  That is true for the short-term, but
agriculture + 500 or so people could allow specialization so that not
everyone would need to hunt/forage for basic necessities.  Then you would have
space for a class of people who could simply invent things.  In fact the
relatively primitive class-based society I just described could to fit the
criteria for an oppressive system that FC is against, and importantly it could
be oppressive *without* large-scale tech.  On the other hand, anyone who was
oppressed would be better able to escape the system by running than someone in
our current all-encompassing systems.

(§§213-230) **The Danger of Leftism** -- I don't want to talk about this in
this post.  He certainly raises some good points about "progressives" and
their psychology, but this section really only deals with how to deal with
them in the revolutionary movement.  I want to focus on the problems with his
assumptions and analysis of industrial society (and possibly human nature).  I
don't care about how the revolution should be organized since I don't agree
with his methods anyway.

---

TODO: critique of kaczinski's ISAIF (the "unabomber manifesto"); what he gets
right and where he misses the mark

Wrong:
* the power process acted out by individuals will always result in the players
  seeking an advantage which cannot but result in technological progress
* scientific curiosity is not a "synthetic power process"; it is rooted in the
  attempt to make sense of the world; this is the source of religion, and I
  think science does a better job than religion
* if modern society is too far out of an individual's control, after the
  downfall of industrial society he will still have to contend with a nature
  who is deaf to his pleas

Right:
* The "progress" of society is less real progress than it is a random walk.
  We pursue certain objectives simply because we can.  There is no ultimate
  destination or ethos that guides our decisions.  Because we have no
  agreement of what the ideal end-goal is, when someone does not sufficiently
  fit into the environment we've created, we assume the problem is the
  individual and not society.<sup>§§143-160</sup>
* There is no room to escape from our society.  If someone doesn't fit in with
  the society, there are only ~150 different countries to choose from and
  maybe only a couple dozen that are governed with any significant difference.
  No one is allowed to just check-out and form their own society.

<br>

#### Footnotes:
