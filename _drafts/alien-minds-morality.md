---
layout: post
title:  "Truly Alien Minds:  Morality"
author: jazzyb
permalink: /alien-minds-morality.html
---

#### Previous post in this series: [Understanding](/alien-minds-understanding.html)

<br>

> TERRI:  The difference between a man and an ape is less than three percent of
> genetic material. But that three percent gives you Mozart. Einstein.
>
> PHIL:  Or Jack the Ripper.
> 
> -- [<u>Mission to Mars</u>](http://www.imsdb.com/scripts/Mission-to-Mars.html)

<br>Differences in morality?  If our ethical values evolved, how might another
species differ?  How did our ethics evolve?  The *natural* ethical framework
evolved to perpetuate the survival of our genes.  Hypothesis:  Any ethical
intuitions will support that goal.  But similar to reason, do ethical biases
exist (that we are not aware of) that interfere with that goal?  Is there a
"better" ethical framework than the one we are evolved to follow?  How would
we even define such a thing?

<br>

#### Footnotes
