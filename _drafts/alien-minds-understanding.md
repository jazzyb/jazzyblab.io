---
layout: post
title:  "Truly Alien Minds:  Understanding"
author: jazzyb
permalink: /alien-minds-understanding.html
---

#### Previous post in the series:  [Reasoning](/alien-minds-reasoning.html)

<br>

> The happy ending is justly scorned as a misrepresentation; for the world, as
> we know it, as we have seen it, yields but one ending:  death,
> disintegration, dismemberment, and the crucifixion of our heart with the
> passing of the forms which we have loved.
> 
> -- Joseph Campbell, <u>The Hero with a Thousand Faces</u>

<br>

1. Humans report and understand events through narrative.  We tell a story by
   picking an arbitrary start and end and emphasize the details in between
   that support the message we want to communicate.
2. Imagine an alien race that reported and understood events differently.  Say
   they reported events in reverse from a conclusion backwards through a
   sequence of inevitable events.
3. To such a species our narratives would seem somewhat dishonest.  They would
   appear to start and stop at arbitrary points.  They would also emphasize
   an illusion of choice.  That at any point an agent *may* have done
   something different.  However, based on their means of reporting, they
   would see all events as inevitable conclusions.  They may not have a
   concept of free will.

<br>

#### Next post in this series:  **Morality**

<br>

#### Footnotes
