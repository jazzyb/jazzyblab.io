---
layout: post
title:  "Cornered by Civilization"
author: jazzyb
---

> The two points selected by which to measure the worth of a form of social
> life are the extent in which the interests of a group are shared by all its
> members, and the fullness and freedom with which it interacts with other
> groups.  An undesirable society, in other words, is one which internally and
> externally sets up barriers to free intercourse and communication of
> experience.
> 
> -- John Dewey, <u>Democracy and Education</u>

<br>

* There is no room to escape from our society.  If someone doesn't fit in with
  the society, there are only ~150 different countries to choose from and
  maybe only a couple dozen that are governed with any significant difference.
  No one is allowed to just check-out and form their own society.
* https://www.youtube.com/watch?v=muf3XYTXfHk
* Joseph Smith and Brigham Young were able to lead a group of people into the
  wilderness to start their own community.  This is impossible today.  The
  majority of Mormons today would have either been jailed or murdered.
* While there might not be anything wrong with people who have been exposed to
  different views saying that they only want to go off and do their own thing,
  that community will one day produce children who are insulated from other
  ideas.  This could lead to a balkanization of society.

<br>

#### Footnotes
