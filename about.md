---
layout: page
title:  About
permalink: /about/
---

This blog is powered by [Jekyll](http://jekyllrb.com/) and [Gitlab Pages](https://pages.gitlab.io/).  Check out the [source code on Gitlab](https://gitlab.com/jazzyb/jazzyb.gitlab.io).

Have any questions, comments, or verbal abuse?  Feel free to file an [issue](https://gitlab.com/jazzyb/jazzyb.gitlab.io/issues).

Interested in my thought process?  Take a look at my [drafts of upcoming posts](https://gitlab.com/jazzyb/jazzyb.gitlab.io/merge_requests?label_name%5B%5D=draft).
