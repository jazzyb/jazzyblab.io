---
layout: post
title:  "Truly Alien Minds:  Introduction"
author: jazzyb
permalink: /alien-minds-intro.html
---

> It is natural to inquire how it has come about that intelligent life has
> evolved on Earth in advance of its appearance on other planets.  Future
> research in such fields as biochemistry; the dynamics of planetary
> formation; and the formation and evolution of atmospheres, may well provide
> a convincing answer to this question.  In the meantime, Fact A [the lack of
> extraterrestrials on Earth now] provides strong evidence that we are the
> first civilization in our Galaxy even though the cause of our priority is
> not yet known.
> 
> -- Michael H. Hart, "Explanation for the Absence of Extraterrestrials on Earth"

<br> If humanity ever does make contact with another intelligent, alien
species, what might that species be like?  Science fiction often presents
aliens as modified hominid bodies with human minds.  They may be feline or
have multiple arms or -- in the case of Star Trek -- have only forehead ridges
to distinguish them from humans, but their psychology and intelligence are,
for storytelling purposes, identical to human minds.  Aliens in science
fiction are essentially reskinned dwarves or orcs from fantasy.

Instead of thinking of all the interesting ways that alien species may have
evolved physiologically on worlds different from our own, in the following
series of posts, I'd like to meditate on what the *mind* of such creatures
might be like, on specifically how *different* they might be from us.  Imagine
that alien spacecrafts land on Earth and attempt contact with us.  How might
that alien species behave?  What might be their cognitive limits and
abilities?  How truly different, cognitively speaking, could such an
intelligence be from us?

#### Table of Contents

1. [Intelligence](/alien-minds-intelligence.html)
2. [Reasoning](/alien-minds-reasoning.html)
3. Next Up:  **Understanding**
