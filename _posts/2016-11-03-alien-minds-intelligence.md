---
layout: post
title:  "Truly Alien Minds:  Intelligence"
author: jazzyb
permalink: /alien-minds-intelligence.html
---

#### Previous post in this series:  [Introduction](/alien-minds-intro.html)

<br>

> The most merciful thing in the world, I think, is the inability of the human
> mind to correlate all its contents.  We live on a placid island of ignorance
> in the midst of black seas of infinity, and it was not meant that we should
> voyage far.
> 
> -- H. P. Lovecraft, <u>The Call of Cthulhu</u>

<br> In the [introduction](/alien-minds-intro.html) I asked the question, "How
truly different could an alien intelligence be from humanity?"  Let's start
with the operative word in that question:  'intelligence'.  When I say
"intelligence" I mean "an optimization process" in the same sense that Eliezer
Yudkowsky uses it:

> Roughly, [intelligence] is the idea that your power as a mind is your
> ability to hit small targets in a large search space - this can be either
> the space of possible futures (planning) or the space of possible designs
> (invention).  Suppose you have a car, and suppose we already know that your
> preferences involve travel.  Now suppose that you take all the parts in the
> car, or all the atoms, and jumble them up at random.  It's very unlikely
> that you'll end up with a travel-artifact at all, even so much as a wheeled
> cart; let alone a travel-artifact that ranks as high in your preferences as
> the original car.  So, relative to your preference ordering, the car is an
> extremely improbable artifact; the power of an optimization process is that
> it can produce this kind of improbability.[^1]

[^1]:  Eliezer Yudkowsky, ["Optimization and the Singularity"](http://lesswrong.com/lw/rk/optimization_and_the_singularity/)

Thus when I say that our hypothetical aliens are "intelligent", I mean:  In
the space of all possible futures, they have a preference for the goal
"spacefaring", and their minds are capable of forging a path through time that
optimizes for the achievement of that goal.  Obviously, all optimization
processes can't be equal.  Some possible intelligences will be less powerful
(dumber) when compared with human minds, and some will be more powerful
(smarter).

Intelligence as an optimization process is not to be confused with IQ[^2].
One person may have an IQ of 100, and another may have an IQ of 120, but as
long as the physical structure of their brains is the same, they are both
capable of the same thoughts even though the person with an IQ of 120 may
arrive at conclusions quicker and with less effort.  By analogy, if
intelligence is a computer program, IQ is the hardware the program runs on.
Say we have two computers running Firefox, but one has more CPUs, more RAM,
and a faster internet connection.  Both will be able to surf the web, but one
will seem "faster".  However, if one computer has Firefox, but the other only
has Notepad, it won't matter how much faster the Notepad computer is; Notepad
simply will not be able to achieve the goal of "surfing the web".  When I say
one optimization process is more intelligent or powerful than another, I mean
that one 'intelligence' may be capable of "goals" or thoughts that the other
is incapable of even conceiving.

[^2]:  I'm using the term "IQ" in this post in the same way that it is
       commonly used in popular culture to mean the "quickness" of one's
       thought.  This popular usage isn't to be confused with the more
       rigorous, academic concept of IQ.  I'm simply a lazy writer and
       couldn't think of a better concept to represent "cognitive speed".

Before we compare the intelligence of our hypothetical aliens with humanity,
we need to take a moment to understand the capabilities of human intelligence.
Humans, as we would recognize them today, evolved in Africa from homo
erectus about 250,000 years ago.  For the half-million years prior, our
distant ancestors had been developing increasingly larger brains.  The
majority of this growth occurred in the temporal lobe, which is the center of
language processing, and the prefrontal cortex, which engages in complex
decision-making and moderating social behavior.  At 70,000 years ago, the
first humans migrated from Africa, and only 10,000 years ago, we invented
agriculture.  Agriculture begot metal tools, permanent cities, and trade and
allowed for the development of our current technological civilization.[^3]

[^3]:  ["Human Evolution" from Wikipedia](https://en.wikipedia.org/wiki/Human_evolution)

None of our closest hominid relatives -- who existed for millions of years
prior -- had the mental capability to achieve anything near our technological
sophistication.  We are the most advanced intelligence that has ever existed
on this planet.  Yet, even though our more recent ancestors were every bit as
mentally capable as we are today, it still took them nearly a quarter of a
million years before they even *began* to jump-start the process of
technological civilization.

This leads me to the following theory:  The optimization process of human
intelligence has the *bare minimum* power required to give rise to a
spacefaring civilization.  Phrased differently:  Humans, collectively, are as
dumb as you can be and still be able to create a technological civilization
capable of space travel.[^4]  If we were to try to approximate the
intellectual limits of our hypothetical, spacefaring aliens, human beings are
the lower-bound of the intelligence range.

[^4]:  I'm only *assuming* humanity is capable of interplanetary or
       interstellar travel since we haven't accomplished it yet, and it
       doesn't sound like an easy task even with infinite resources.  Until we
       prove that we can achieve interstellar travel, some may say that any
       species who can is *certainly* more intelligent.

To clarify:  I am not making the claim that an average IQ of 100 is the
minimum lower-bound.  I am claiming that the capabilities of our 'optimization
process' are the bare minimum required.  For example, I believe a human
society whose average IQ happened to be 80 or 90 could also achieve the same
level of technological civilization that we have, albeit at a possibly slower
rate of progress.  I assume homo erectus or chimpanzees, on the other hand,
couldn't do the same even if given infinite time.

Corollary to the theory:  Any species that is capable of traveling to our
world across the vast distances of space is not only more technologically
advanced than humanity but is *probably* more intelligent.

But how much "more intelligent" are we talking?  What is the upper-bound?
This is where the word 'probably' applies because it may be possible that
human intelligence is as powerful an optimization process as can be achieved.
If this were the case, then any spacefaring race would be equal in
intelligence with humans, give or take some IQ points.  However, I find it a
bit arrogant to believe that human evolution has just happened upon the most
powerful optimization process possible; if it wasn't true for neanderthals or
homo erectus, I doubt it's true for us.

If there is a continuum of optimization processes, the upper-bound could be
'unbounded'.  That's not a terribly meaningful answer, but perhaps we could
try to understand the possible scale.  Take Isaac Newton; he was arguably the
most brilliant human mind to ever exist.  It may have taken his great power of
intellect to discover calculus[^5], but your average teenager (provided he has
a sufficiently effective teacher) is capable of following the reasoning and
arriving at the same conclusions over the course of a few weeks.  By
comparison, assuming we could remove the language barrier and develop a
sufficiently large vocabulary of signs to communicate the concepts of calculus
to a chimp, the smartest chimp who ever lived would be unable to begin to
comprehend calculus.  The level of abstract thought required for such a feat
is too far beyond the power of his intelligence.

[^5]:  Ignoring Leibnitz's contribution for the purpose of the analogy.

Such is the situation we would probably find ourselves in if an alien being
tried to communicate with us.  There may be truths we cannot think, not just
because we can't discover them on our own, but because our minds lack the
functionality to follow the most straight-forward explanation laid out before
us.  If there is other intelligent life in the universe, that life might note
only superficial differences between the intellects of humans and apes.

<br>

#### Next post in this series:  [Reasoning](/alien-minds-reasoning.html)

<br>

#### Footnotes
