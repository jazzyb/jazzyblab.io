---
layout: post
title:  "The Importance of Ethical Intuitions, Models, and Machines"
author: jazzyb
---

> The striking feature of the utilitarian form of justice is that it does not
> matter, except indirectly, how this sum of satisfactions is distributed
> among individuals any more than it matters, except indirectly, how one man
> distributes his satisfactions over time.
>
> [...]
>
> Utilitarianism does not take seriously the distinction between persons.
> 
> -- John Rawls, <u>A Theory of Justice</u>

<br>

### Introduction

Just as we can evaluate the truth or falsehood of ideas independently of our
personal beliefs using logic or empiricism, it seems like we should have
similar means of evaluating the moral rightness or wrongness of actions
independently of our moral intuitions.  Philosophers have developed a number
of normative ethical theories which attempt to provide a framework from which
to make unbiased moral judgments, but none have proven universally
satisfactory primarily because we cannot agree on a standard outside our moral
intuitions to measure them against.  When our ethical models cause us to draw
conclusions that run counter to our moral intuitions, we are left wondering
whether it is the model or our intuitions that are in error.

Recently, one particular utilitarian thought experiment has brought this
concern to my attention.  In the post below I intend to present this ethical
problem, explain why I think the "obvious" conclusion is morally unsatisfying,
and emphasize the practical reasons for answering these sorts of ethical
questions correctly in the future.

### Torture vs. Stubbed Toes

Eliezer Yudkowsky once suggested a moral thought experiment[^1] in which you
must decide between two options:  Either you can allow one person to be
tortured for fifty years or you can allow a googolplex of people to stub
their toes.[^2]

[^1]:  Eliezer Yudkowsky, ["Torture vs. Dust Specks"](http://lesswrong.com/lw/kn/torture_vs_dust_specks/)

[^2]:  This ethical thought experiment predates Yudkowsky.  Alastair Norcross
       suggests a similar thought experiment in his essay "Comparing Harms:
       Headaches and Human Lives".  Some of the counter-arguments I discuss
       later are adapted from Norcross's essay.

So... do you choose to torture or to stub toes?

The answer might seem obvious to you, but it's worth taking a moment to
understand the scales involved in the question before jumping to a decision.
Yudkowsky chose fifty years of torture on the one end of the scale because it
"is one of the worst things that can *realistically* happen to one person in
today's world."[^1]  On the other end he offered the smallest noticeable
amount of pain that one person could experience.[^3]  The catch is that he
multiplied that tiny pain across an unfathomable number of people.  A googol
is 1 followed by one hundred zeros (10<sup>100</sup>) which is many orders of
magnitude more than the estimated number of atoms in the known universe.  A
googolplex is 1 followed by a googol zeros
(10<sup>10<sup>100</sup></sup>).[^4]

[^3]:  In the original version of the thought experiment, Yudkowsky proposed
       that getting a dust speck in one's eye was the least pain that could be
       experienced.  A lot of commenters seemed to get caught up on the idea
       of dust specks even registerig on the pain scale.  Feel free to replace
       "stubbed toes" with whatever you feel is the minimum amount of
       nontrivial pain -- head aches, needle pricks, sprained ankles, etc.

[^4]:  The original version used the number 3↑↑↑3 which is based on [Knuth's
       up-arrow
       notation](https://en.wikipedia.org/wiki/Knuth%27s_up-arrow_notation).
       The value of 3↑↑↑3 isn't worth taking time to explain in this post.
       Any practically uncountable amount will do -- hence, a googolplex.

For Yudkowsky -- a utilitarian -- the answer is obvious: You should condemn
the one to fifty years of horrible torment in order to save the many from the
collective pain of stubbed toes.  And yes, he sincerely believes that is the
right thing to do.

A quick background in utilitarianism should help clarify his position:
Utilitarianism is an ethical view that is informally understood as preferring
actions which maximize the 'utility' of sentient minds.  'Utility' is a vague
stand-in for "general well-being" -- sometimes characterized as pleasure or
satisfaction.  Essentially, a utilitarian views an act as "good" when it
raises the overall level of satisfaction in the universe and "bad" when it
causes more dissatisfaction.  Ethicists disagree on exactly what the abstract
concept of utility should represent and how one should measure it (which I
will address later), but this simple explanation should suffice as a starting
point for understanding Yudkowsky's position.

Yudkowsky assumes that there is some utility function `u` which takes an
experience (e.g. 'sex' or 'falling off a ladder') and measures the value of
the utility for that experience.  For example, 'sex' would presumably increase
one's well-being so `u(sex) = 20`, but 'falling off a ladder' is bad so
`u(falling off a ladder) = -100`.  (Don't get too caught up on the values;
they're mostly arbitrary other than "pleasurable" things are represented by
positive numbers and "painful" are negative.)  Yudkowsky's evaluation of the
thought experiment then looks something like:

```
		u(torture) > u(stubbed toe) * googolplex
```

Notice that it really doesn't matter what the utilities are for the two
experiences because as long as `u(stubbed toe)` is any negative utility then
the shear number of sentient minds affected on the right-hand side will
out-weigh the negative utility of the torture.  Yudkowsky admonishes those
callous individuals who would choose stubbed toes over torture to just "shut
up and multiply!"[^5]  If you still feel that `u(torture)` is less than
-(10<sup>10<sup>100</sup></sup>), then just exponentiate a googolplex until
you have a sufficiently large number to out-weigh the single pain of torture,
and Yudkowsky's preference for torture holds.  His argument is that any pain
experienced by one person can be out-weighed by the sum of smaller pains over
enough individuals.

[^5]:  Eliezer Yudkowsky, ["Circular Altruism"](http://lesswrong.com/lw/n3/circular_altruism/)

I think most people, even utilitarians like myself, find something deeply
*wrong* with Yudkowsky's conclusion.  The result doesn't jive with our moral
intuitions.  A number of very intelligent people have expressed their own
problems with Yudkowsky's reasoning, and I think it's worth examining some of
the more common counter-arguments before I explain my own reasons for
disagreeing.

A couple of points are worth mentioning, though, before we examine arguments
against Yudkowsky's conclusion:

First, the thought experiment only takes into account *direct* pain caused by
the experiences.  If a googolplex of people stub their toes in the real world,
the risk of infection alone is sufficient to result in *billions* of deaths.
If we took the risks of fatalities under consideration, I think most people
could be persuaded to choose torture over stubbed toes.  However, Yudkowsky is
careful to clarify in subsequent comments that the thought experiment does not
address any repercussions due to the experience -- i.e. neither fatal
infections nor torture-induced PTSD are to be included in our considerations.

Second, this post will only examine *utilitarian* counter-arguments to
Yudkowsky's conclusion.  Moral relativists might argue that the question is
meaningless, but we assume that there is an objectively correct answer.
Likewise, there are valid deontological counter-arguments that would state
that it is simply impermissible to torture in order to avoid a lot of mild
annoyances, but Yudkowsky explicitly offers the thought experiment to other
utilitarians in order to contrast our moral intuitions with (what is in his
opinion) "real altruism".  Thus, any counter-argument that does not rely on
the central premise of 'utility' is beyond scope.

### Counter-Argument #1:  *Incomparability*

One of the more common arguments I noticed in the comments to Yudkowsky's
thought experiment is what I will call the *Incomparability Argument*.  It
goes something along the following lines:

> There are levels of disutility which are distinct from one another and,
> indeed, are incomparable to one another.  Thus, no number of stubbed toes
> could be equivalent to torture.

I don't think the commenters who support this argument mean that torture and
stubbed toes are *strictly* incomparable -- that would imply that stubbed toes
are neither worse than, better than, nor equivalent to torture.  What they
mean to say is that N units of stubbed toes are a better outcome than 1 unit
of torture for all values of N.

Advocates of this argument imagine a stair-step of levels of disutility
wherein each level contains various pains of the same "scale".  For example,
the bottom rung would include 'stubbed toes', 'needle pricks', and
'headaches', and the top rung would include 'torture', 'maiming', and 'death'.
Between these two extremes exist rungs of increasingly dissatisfying
experiences.  Within each rung there is an order, and one can find
equivalencies between experiences.  For example at the bottom rung, one may
say that 10 different people experiencing a headache is worth one person being
pricked by a needle, and 10 needle pricks is equivalent to one stubbed toe,
but one person tortured is always worse than any number of people experiencing
stubbed toes.

The problem with this counter-argument lies at the boundaries between levels.
Say that we measure the bottom rung to be from 0 to -50 units of utility, and
the next rung begins at -51 units.  We would be comfortable saying that two
people experiencing -25 units of pain each is exactly as bad as one person
experiencing -50 units of pain, and we would prefer one person to experience
-50 units of pain rather than three or more people experiencing -25.  But it
seems strange to say, within the same model, that no number of people
experiencing -25 units of pain can ever be equivalent or worse than one person
experiencing -51.  Something seems inconsistent between this idea and its
realization.

### Counter-Argument #2:  *Fairness*

I call the next argument the *Fairness Argument*.  This argument was brought
up a couple of times in the comments with the following reasoning:

> "Fairness", or relative equality of utility, is as important -- if not more
> so -- than aggregating utility by total or average.  Acts should be chosen
> which favor smooth curves of utility across a population over small spikes
> of extreme utilitarian inequity.

Advocates of this argument reject 'torture' because of the extreme relative
difference in utility between the individual being tortured and the rest of
the population.  They prefer 'stubbed toes' because it represents a perfectly
equal distribution of utility.  Classical utilitarianism (as far as I know)
only measures the aggregate utility of society via sum or average.  I'm not
aware of any utilitarian ethicist who advocates any method of taking equality
under consideration.

The classical utilitarian position with respect to 'utility' has some
intuitive justification -- overall well-being seems like a perfectly
reasonable attribute to maximize for in most cases.  Introducing the
additional premise of 'fairness' into the theory seems to imply that
'fairness' is a good end in itself, and this does not match my intuition.
'Fairness' is to me only good in so far as it increases the overall
well-being; it is only a means to greater utility.

However, this argument does highlight the problem utilitarianism has with
aggregating utility.  I will expand further on this idea below, but it is
worth mentioning here that the assumption that one should merely sum or
average individual utility to represent the "health" of a population often
results in repugnant conclusions to ethical dilemmas.  The *Fairness Argument*
at least tries to fix such problems.

### Counter-Argument #3:  *Preference*

The next counter-argument I'll explore is one I call the *Preference
Argument*.  This argument was typically phrased as follows:

> Virtually all of the participants of this thought experiment would prefer to
> stub their toe if they knew the alternative would be for someone to be
> tortured.  Given that, there is no reason to choose torture over stubbed
> toes.

There are, for lack of better words, what I call the 'weak' and the 'strong'
forms of this argument.  In the 'weak' form the speaker is subtly trying to
change the thought experiment into one of:  "If you had the choice to stub
your toe or to condemn one person to fifty years of torture, what would you
do?"  Most people given this scenario would take the stubbed toe because
stubbing one's toe is clearly preferable to allowing someone else to be
tortured, but this is an entirely different thought experiment from
Yudkowsky's.  The original is asking you to be an impartial, objective
observer and make the right decision on behalf of all the participants.
Changing it in this way is like posing the famous "Transplant Problem"[^6] but
instead asking if you would be willing to sacrifice yourself to save the five
patients.

[^6]:  From Judith Jarvis Thomson's essay "The Trolley Problem":  "A brilliant
       transplant surgeon has five patients, each in need of a different
       organ, each of whom will die without that organ. Unfortunately, there
       are no organs available to perform any of these five transplant
       operations. A healthy young traveler, just passing through the city the
       doctor works in, comes in for a routine checkup. In the course of doing
       the checkup, the doctor discovers that his organs are compatible with
       all five of his dying patients. Suppose further that if the young man
       were to disappear, no one would suspect the doctor. Do you support the
       morality of the doctor to kill that tourist and provide his healthy
       organs to those five dying persons and save their lives?"

On the other hand, there is a 'strong' version of the argument which raises
questions about what 'utility' actually means.  I have equated 'utility' in
most of this post with satisfaction or pleasure (called "hedonistic
utilitarianism"), but there is another form of utilitarianism called
"preference utilitarianism".  This variant of utilitarianism equates 'utility'
with 'preference'.  As in, it promotes actions which fulfill the interests of
the individuals involved, which may or may not concern their personal
well-being.

Most of the time there are strong correlations between what people want
(preferences) and what people like (pleasures), but occasionally there are
distinctions.[^7]  Imagine, for example, that there were a machine which
produced indescribable pleasure for the people hooked up to it, and
additionally it would keep the subject alive and healthy.  The purely
hedonistic utilitarian would be hard-pressed not to condone rounding unhappy
people up and forcing them into the machine.  A preference utilitarian on the
other hand might take into account an individual's personal preference for
autonomy, and it certainly wouldn't be as obvious that forcing a person into a
pleasure machine against their will would be the right thing to do.

[^7]:  And in fact there is some reason to believe that 'pleasure' and
       'preference' are controlled by distinct mechanisms in the brain.  See
       ["Dissecting Components of Reward: 'Liking', 'Wanting', and
       Learning"](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2756052/) by
       Kent C. Berridge et al.

Returning to Yudkowsky's conclusion that we should choose torture, we would
find ourselves having created a world in which virtually *every participant*,
the tortured and the others, would prefer that they existed in the universe
with stubbed toes.  In this case I would say that when we map 'utility' onto
individual preferences instead of satisfaction -- and it isn't obvious to me
that satisfaction makes more intuitive sense than preference -- the obvious
choice is stubbed toes.  This indicates that the error may not be with our
intuitions as Yudkowsky would have us toe-stubbers believe but rather with
Yudkowsky's model failing to accurately represent ethical reality.

### Counter-Argument #4:  *Distribution*

I call the last counter-argument we'll examine the *Distribution Argument*.
It is best stated in the comments by the [LessWrong](http://lesswrong.com)
user Hul-Gil:

> [S]uppose I steal one cent each from one billion different people, and
> Eliezer steals $100,000 from one person. The total amount of money I have
> stolen is greater than the amount that Eliezer has stolen; yet my victims
> will probably never even realize their loss, whereas the loss of $100,000
> for one individual is significant. A cent does have a nonzero amount of
> purchasing power, but none of my victims have actually lost the ability to
> purchase anything; whereas Eliezer's, on the other hand, has lost the
> ability to purchase many, many things.[^8]

[^8]:  [http://lesswrong.com/lw/n3/circular_altruism/6hml](http://lesswrong.com/lw/n3/circular_altruism/6hml)

This example contains aspects of the previous arguments, but more than the
others, it emphasizes the distinction between individuals that Yudkowsky does
not make in his conclusion.  Yudkowsky responds to Hul-Gil in the comments:

> Isn't this a reductio of your argument? Stealing $10,000,000 has less
> economic effect than stealing $100,000, really? Well, why don't we just do
> it over and over, then, since it has no effect each time? If I repeated it
> enough times, you would suddenly decide that the average effect of each
> $10,000,000 theft, all told, had been much larger than the average effect of
> the $100,000 theft. So where is the point at which, suddenly, stealing 1
> more cent from everyone has a much larger and disproportionate effect,
> enough to make up for all the "negligible" effects earlier?[^9]

[^9]:  [http://lesswrong.com/lw/n3/circular_altruism/7vcb](http://lesswrong.com/lw/n3/circular_altruism/7vcb)

I do want to clarify that Hul-Gil never said stealing a penny from each person
"has no effect each time".  He said it isn't zero, *but* it hasn't affected
anyone's purchasing power.  Hul-Gil would probably admit that there is some
point, call it $X, where stealing $100,000 from one person is better than
stealing $X from one billion.  He admits that he is separating "disutility
into trivial and non-trivial categories".  In doing so, he is succumbing to
the same problems as the *Incomparability Argument*.  However, his focus on
"purchasing power" brings to the fore the impact the action will have on the
*preferences* of the participants.  If we approach the problem from the
perspective of preference utilitarianism, we reach agreement with Hul-Gil:  A
loss of one cent will not impact the realization of the preferences of any
individual, but a loss of $100,000 would surely impact the realization of one
person's preferences.  To answer Yudkowsky's challenge from a preference
utilitarian position, the repeated $10,000,000 theft becomes a problem when it
impacts more people's interests than the repeated $100,000 would have done.

### Aggregating Utility

The *Distribution Argument*, more than any of the others, emphasized the
distinction between individuals.  We are not justified in burdening one person
with weights that could have been distributed without burden among others.
While I think the concept of utility (measured as either interests or
well-being) makes intuitive sense, the idea that it should be summed or
averaged across a population does not have the same intuitive justification.
Utility exists in sentient minds, but the universe or a population of sentient
minds does not have a utility of its own.  Hul-Gil's example emphasizes the
problem of failing to take into account *how* a particular amount of
disutility is shared.  But what other ways can we calculate utility other than
summation or average?

I suspect a good place to start is in Game Theory.  Imagine that every
individual in the population has their own particular utility function which
represents their preferences.  This is similar to preference utilitarianism
except instead of aggregating utility, we treat all the individuals like
players in a game and try to find something akin to a Nash or correlated
equilibrium.  Equilibrium points in this model would be states in the game
where each player's utility is being satisfied as best it can without
impacting the preferences of the other players.  Put more naturally, the
optimal action is one which allows people to optimize for their personal
preferences without interfering with the same effort by others.  This method
of aggregation could lead to a utilitarianism that actually distinguishes the
agents involved and bears some resemblance to the theory of natural rights in
which one person's rights stop where the rights of others begin.

Admittedly, I don't yet know what the mathematics would look like or what
problems the model will have, but the initial, rough idea seems promising.
When I find the time, I intend to explore this idea further.

### The Importance of Machine Ethics

Regardless of whether or not my own theory of utilitarianism via equilibrium
is sound, I believe I have shown where Yudkowsky's answer to his thought
experiment fails and some potentially better ways of approaching the problem.
However, even though I think I have the correct answer, I admit I could be
wrong.  Perhaps Yudkowsky's model is accurate, and he is right in believing
that our intuition is in error.  Perhaps preference utilitarianism as I have
presented it has serious structural problems that are only fixed by a
hedonistic model.  What then?

You might ask why any of this matters.  Yudkowsky's thought experiment is so
far beyond what is possible in the real world that this debate might seem
silly.  Of course, most human beings know how to behave morally.  We might
quibble over the theoretical details, but when another person's life is on the
line, most of us at least know the right actions to take even though we might
lack the courage to act correctly.

If you want to write a computer program to play grandmaster-level chess, you
will need to understand and implement efficient data-structures like bitboards
and minimax search algorithms.  If you want to become a chess grandmaster, you
would be wasting your time studying data-structures and algorithms.  The human
mind already implements effective strategies for anticipating your opponent's
next moves, but a computer must be told *explicitly* how to reason about a
board position.  All of the abstract processing structures that we take for
granted in our gray matter must be built from scratch in silicon.

As artificial intelligence improves, we will increasingly give AI programs
goals and expect them to figure out the steps to achieve those goals on their
own.  The more autonomy we allow them, the more we will rely on them behaving
ethically.  If we give a sufficiently autonomous AI the vague command to "cure
cancer", that AI needs to know that the easiest way of eliminating cancer
isn't to eliminate the host or that testing possible cures on humans against
their will isn't acceptable.  The programmers of such an AI won't be able to
hard-code an ethical rule for every conceivable interaction.  The AI will have
to be designed to figure out how to treat humans on its own.

I don't think AI of this sort will be developed in the near-term, but I do
think technology is advancing at such a rate that we might encounter this
problem in the next few centuries.  We will need to understand ethics and get
it *right* before this sort of AI comes into existence.  Imagine that a
powerful machine is programmed with Yudkowsky's hedonistic utilitarianism.
Would you feel comfortable with its judgment?  On the other hand, if I am the
one who is incorrect, if my intuitions are wrong about the damage I am
inflicting on people, we should not want the AI to share my views either.  Our
future robot overlords will need to be programmed with a correct ethical
framework from the start.

<br>

#### Footnotes
