---
layout: post
title:  "Why I Am Not a Unitarian Universalist"
author: jazzyb
---

> Religion must remain an outlet for people who say to themselves, "I am not
> the kind of person I want to be."  It must never sink into an assemblage of
> the self-satisfied.
> 
> -- Frank Herbert, <u>Dune</u>

<br>

### My Journey to Unitarian Universalism

I grew up in a charismatic, fundamentalist, evangelical Christian church
in rural Tennessee.  Theologically, it was pretty much what you'd imagine --
minus the snakes.  Contrary to the common hypocritical stereotypes, my church
community was in large part made-up of honest, intelligent, kind folks.  I
would not be what I am today (the good parts anyway) without their influence.

Progressively over the course of my teenage years, I became less and less
convinced of the truth of Christianity until I finally left the Church.  I
might discuss the details of my deconversion in a future post.  Just know that
my break with the Christian faith and my eventual atheism had nothing to do
with any negative experiences from my church family nor did it have anything
to do with me wanting to "live in sin".  I wanted to seek truth and to speak
truthfully, but I was no longer convinced that Christian scripture contained
any more truth than any other book.

Several years after I lost my faith I moved to Texas and there started
attending a Unitarian Universalist (UU) church.  I had no experience with the
UU religion until I walked through the doors.  I began attending because I
missed having a church community (having spent most of my life with one) and
assumed (rightly) that the UU church would be a much more open-minded version
of the Christian tradition that I was familiar with.

### The Unitarian Universalist Ideal

I began learning about what UUs believed.  The closest the UU religion has to
an official creed are the Seven Principles[^1]:

1. The inherent worth and dignity of every person;
2. Justice, equity and compassion in human relations;
3. Acceptance of one another and encouragement to spiritual growth in our
   congregations;
4. A free and responsible search for truth and meaning;
5. The right of conscience and the use of the democratic process within our
   congregations and in society at large;
6. The goal of world community with peace, liberty and justice for all;
7. Respect for the interdependent web of all existence of which we are a part.

[^1]: [Our Unitarian Universalist Principles](https://www.uua.org/beliefs/what-we-believe/principles)

As you can imagine, such a liberal theology invites an awfully broad range of
spiritual beliefs.  Within the church questions of spiritual belief are a
personal choice, i.e. everyone is free to decide for themselves what they
believe with respect to deities, the meaning of life, etc.  I attended church
with many self-proclaimed Jews, Muslims, Christians, Wiccans, and Humanists,
and I'm sure the congregation contained even more spiritual variety than I was
personally exposed to.

Such open-mindedness, ideally practiced and supported by a strong ethical
foundation, should be spiritually edifying.  Members of the church are exposed
to and have the opportunity to draw from a wide range of religious and
cultural traditions.  One's own spiritual beliefs can be constantly measured
against those of others without combativeness in an environment of acceptance.
No one's spiritual beliefs need be based on any sort of rigid dogma and,
therefore, can be more easily changed when new knowledge or arguments present
themselves.

It's important to note that UUism doesn't support all beliefs without limit.
Even though the UU religion allows a broad range of spiritual beliefs, the
church teachings take a narrower stance with respect to ethical matters.  A
member could not, for example, support a racist ideology and still be a UU.
Similarly, to hate another person for their sexual orientation or to refuse to
help the poor would be to go against the core of UUism.  Of course, one may
fairly say that such stances are the bare minimum of ethical beliefs, but the
point to take away should be that the members of the church *lived* those
ethical beliefs.  The church itself provided food and shelter for the homeless
on cold nights, and most of the people I met there were heavily involved in at
least one charity.  They were good people who wanted to help others.

### The Spiritual Attitude

The longer I attended the church and watched how UUs interacted, the more I
noticed how the liberal theology the church practiced actually ran counter to
how I interpreted the third and fourth principles.  In particular, members of
the church never actually *discussed* their beliefs with one another; they
*shared* their beliefs.  Imagine a scene in which a number of members are
talking after service:  A Muslim might say something like, "There is only one
God, and Muhammad is his prophet."  Then a Humanist would state that he didn't
believe in any gods.  And finally, a Wiccan would expound at length on how
ultimately everything is God because everything in the universe contains "a
spark of the divine".  Then after a brief pause everyone would nod
thoughtfully and avoid mentioning the obvious conclusion that they all
couldn't be right.  Conversation of this sort is not aimed at finding truth;
it is the opposite of "encouragement to spiritual growth".

Not once, the whole time I attended, did anyone ever offer their reasons for
believing as they did, nor did anyone ever ask another to justify their
beliefs.[^2]  In practice such an environment did not encourage one to "search
for truth and meaning", responsibly or otherwise; it encouraged spiritual
stagnation and complacency.  The thing is, while there certainly is such a
thing as personal meaning, there is no such thing as personal truth.  Reality
is true, and there is only one reality.  The search for truth is the process
of aligning our beliefs with reality.  This process is of course important
because it is how we determine not just right from wrong but what we should do
with our lives.  It is how we move beyond just our bare minimum ethical
responsibilities.  It helps us understand our and others' place in the world.

[^2]:  The only time anyone ever came close to challenging my beliefs was when
       I told another man that I was trying to read the Bible cover to cover.
       A look of disgust fell over his face, and he responded, "What would you
       ever want to do that for?!"

That's not to say that an environment which merely accepts all spiritual
beliefs is wrong for everyone.  I met a lot of people while I was in the
church who had been scarred by other religions and other churches.  They
didn't need their beliefs challenged.  These people just needed a community
that would accept them for who they were.  They were wounded and needed a
place to heal, and I think the UU church was the perfect place for them at
that point in their lives.  I do believe, though, that a place lacking in
spiritual challenge is not where anyone should remain indefinitely.  One's
religious or philosophical beliefs should determine the direction of one's
life, not the other way around.

### The Political Attitude

Services in our church always started with either a lay-leader or our pastor
stating that our church was an inclusive church which welcomed everyone
regardless of gender, race, religion, sexual orientation, etc.  The welcome
would go on and on, and sometimes the speaker would have a bit of fun with it.
On one occasion our pastor went off-script and ended the welcome with "...and
we even welcome Republicans."  And the congregation laughed.  Because it was a
joke.

Nearly all of the congregation is made-up of politically left-leaning members.
This is unsurprising because one would assume that a religion which champions
the acceptance of a broad range of spiritual beliefs would also champion a
number of feminist, LGBT, and social equality issues which tend to be
supported on the left-end of the political spectrum.  There is, however, a
silent minority of secret conservatives within the church.  They remain secret
and silent because to openly admit that they support *some* conservative
social or economic policies would be to risk being ostracized by their church
family.

I had many conservative friends while I was in Texas.  A few of them were
Republicans who tended, spiritually, towards a more liberal view of
Christianity.  In some cases they were dissatisfied with their church, and I
knew that *spiritually* they would have thrived in UUism.  But I never would
have suggested that they visit the UU church.  As much as they might have
enjoyed the service, they would have been turned off within ten minutes of
fellowship with politically intolerant liberals who assume all conservatives
are evil or stupid or both.

It amazes me how a community which is so spiritually open-minded could be so
politically narrow-minded.  Most of the members of the congregation have never
considered that a pro-life advocate might reasonably extend "the inherent
worth and dignity of every person" to an unborn child or that the death
penalty -- responsibly and judiciously used -- might support, rather than
undermine, "*justice* ... in human relations" or that there may be no
contradiction in supporting strong national borders and still hoping for "the
goal of world community".  I'm not saying that all these political stances are
right or that all UUs should support them, but surely a community which claims
to support "the right of conscience and the use of the democratic process" can
recognize the danger of a political echo-chamber.

### Unitarian Universalism Can Do Better

As much as I criticize UUism, I have few concerns with it as a religious or
spiritual ideal.  The Seven Principles need no justification.  However, I
think the UU church could create a healthier spiritual environment for its
members if it promoted better spiritual practices.

**The first of these practices is to be more politically inclusive.**  There
are perfectly moral people on the right-end of the political spectrum who have
good, *ethical* reasons for their political views.  Many of them are looking
for the kind of spiritual home that the UU church wants to be, and the church
should welcome them.

**The second practice the church should encourage among its members is to
promote reason instead of faith in one's search for truth and meaning.** Blind
faith simply does not reliably produce beliefs that align with reality, and
reason is the best defense against dogma.  This doesn't necessarily mean that
everyone in the church should have the same spiritual or metaphysical views,
but members of the church should *politely* challenge one another to have
sound reasons for believing what they believe -- especially concerning
spiritual matters.

I'm confident that encouraging these changes among the congregation would help
better align the practice within the UU church with UUism's ideals.

<br>

#### Footnotes
