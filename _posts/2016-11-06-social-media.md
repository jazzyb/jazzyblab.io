---
layout: post
title:  '"Connecting" in the Age of Social Media'
author: jazzyb
---

> One hears people say with some considerable enthusiasm that in the future,
> putting television, computers and the telephone together, people will be
> able to shop at home, vote at home, express political preferences in many
> ways at home so that they never have to go out in the street at all and
> never have to meet their fellow citizens in any context because we'd have
> this ensemble of technologies that keep us private, away from citizens.
> 
> -- Neil Postman, [C-SPAN Booknotes interview](http://www.booknotes.org/FullPage.aspx?SID=31627-1), 30 August 1992

<br> There is a game I like to play -- typically with news headlines -- that I
call "How-Would-the-18<sup>th</sup>-Century-Interpret-This".  The way you play
this game is to read a statement and imagine how an educated, colonial
American from the 18<sup>th</sup> century might make sense of it.  Most of the
time the tech headlines I peruse merely result in humorous gibberish --
headlines like ["Open Sourcing a Deep Learning Solution for Detecting NSFW
Images"](https://yahooeng.tumblr.com/post/151148689421/open-sourcing-a-deep-learning-solution-for)
or ["Programming quantum computers using 3-D puzzles, coffee cups, and
doughnuts"](https://arxiv.org/abs/1609.06628) -- but occasionally I find a
statement that gives me pause.

In a 2015 Bloomberg interview, Mark Zuckerberg outlined Facebook's plan to
provide internet service to as much of the world as they can manage.  In the
middle of the interview Zuckerberg stated:

> [T]here's this mission belief that connecting the world is really important,
> and that is something that we want to do.  That is why Facebook is here on
> this planet.[^1]

[^1]:  [Mark Zuckerberg Q&A: The Full Interview on Connecting the World](https://www.bloomberg.com/news/articles/2015-02-19/mark-zuckerberg-q-a-the-full-interview-on-connecting-the-world)

Play along with me:  Take a moment to ask yourself how, without any context,
someone two hundred or more years ago might have interpreted the above
declaration.

The first question that comes to my mind is, What might the phrase "connecting
the world" mean to someone who is completely ignorant of the internet?  I
would imagine that to say one person is 'connected' to another would imply
that there is some non-trivial amount of intimacy between those people, that
they not only understand but deeply care about one another.  In which case "to
connect the world" would mean to generate this sense of empathy across
countries and creeds.  Of course, we modern folk know that when Zuckerberg
says "connecting the world" what he really means is "connecting *the people
of* the world *to the internet*"; the verb, 'to connect', has taken on an
extra meaning in the 21<sup>st</sup> century.  That's not to say that either
type of connection is necessarily better.  While I would certainly prefer the
world to be more connected in the older sense of the word, I also recognize
the economic and educational opportunities that the newer usage can provide.

I become concerned, however, when I see the two meanings being conflated.
Zuckerberg starts the same interview with this statement:

> When people are connected, we can just do some great things. They have the
> opportunity to get access to jobs, education, health, communications. We
> have the opportunity to bring the people we care about closer to us.

Between the second and third sentences, Zuckerberg has seemingly shifted from
the older meaning of connection to the newer without acknowledging that he is
suggesting the same solution will solve two different problems.  I doubt he is
even aware of the slip, and he is not the only social media guru to make this
semantic mistake.  Former Twitter CEO, Dick Costolo, when asked about
Twitter's vision, replied, "We want to instantly connect people everywhere to
what's most important to them."[^2]  The implicit message being that the
problem Twitter wants to solve -- distance from what's important to us -- is a
technical one.  And this is the real danger with such language:  We can be
fooled (and often are) into thinking that the same technical solutions
required to network computers must also be sufficient to solve *human*
problems related to social capital and empathy.  While the internet connects
computers, it doesn't necessarily connect people -- at least not in the older
sense of the word.

[^2]:  [Twitter is about ‘connecting people for a purpose’ says CEO](http://www.telegraph.co.uk/technology/twitter/8252422/Twitter-is-about-connecting-people-for-a-purpose-says-CEO.html)

We should recognize that 'connecting' is a poor way of describing most
interactions between people through the internet.  We are no more 'connected'
to our Twitter followers or YouTube subscribers or Facebook "friends" than a
Hollywood starlet is connected to her fans.  We might share our thoughts; we
might perform for them; but none of those superficial interactions mean the
people consuming our content know us in any meaningful sense.  Instead, the
term we should be using is **'broadcasting'**.  Social media, as it currently
exists, is better understood as a platform for people to broadcast their
message or identity (or the identity they wish us to perceive) onto the world.

I imagine the new social media mavens would disagree with my assessment:
"Broadcasting", the straw-man may say, "is a one-way form of communication.
Don't most social media platforms provide means of generating *conversation*
among people, and doesn't this promote connection?  Take YouTube for example:
Obviously, a video is a one-way broadcast, but the YouTube platform provides a
means for consumers of the video to converse about the content in comment
sections.  Producers of content can connect directly with their subscribers
and get instant feedback.  Doesn't this count as connecting people in every
sense of the word?"

Yes, I know, YouTube comment sections are an easy target to tear down, but
I'll meet the argument half-way.  Pretend for a moment that YouTube comment
sections weren't the wretched hive of scum and villainy that we are all
familiar with, that YouTube was a utopia where everyone was polite and slow to
anger.  In such a perfect world, would the form of the technology encourage
people to better connect to one another?  I argue that it would not, that
comment sections -- and this includes all comments, not just those on YouTube
-- encourage broadcasting of identity over understanding.  The medium
encourages commenters to reply in short, pithy statements (and in many cases a
karma race is applied which incentivizes posting something, anything quickly
for upvotes rather than risking having a long-form idea buried in the thread).
These short statements are usually made in opposition to or support of the
original post, and rarely do such posts promote a better understanding of the
topic.  For example, questions for clarification of a point in the original
post are unlikely to be sincere and are typically phrased in a way that
criticizes or challenges the point of the original poster without offering a
better alternative.  Comment threads become a stage where commenters are more
interested in their audience than the argument.[^3]

[^3]:  Anecdotally, I find that the fewer users involved in a discussion
       thread, the more likely it is to have genuinely insightful comments.

But comments are only one aspect of social media platforms (even if the most
interactive).  What about "online communities", groups of like-minded
individuals who are too few and far between in the real world to meet but who
can easily connect online; or what about online groups that raise awareness of
issues more efficiently than could be done before the internet?  Don't such
communities and groups encourage human connection?

In general, no.  Because membership in such communities or groups can be
accomplished with nothing more than a mouse-click, no one is required to *do*
anything in, e.g., a Facebook group or a subreddit, only to "support the
cause".  Membership in the group thus becomes only a signal of what one
identifies with.  When these groups do produce social capital, it is often
because they meet-up offline to engage with one another.[^4]  Robert Putnam
raised a similar issue of lack of civic engagement at the turn of the century
in his book <u>Bowling Alone</u>.  In it he argues that there has been an
increase in 'observers' (or fans) within organizations and a decrease in
'doers'.  Putnam raised this issue about organizations like the NRA and
National Wildlife Federation long before social media took off, but I believe
his observations about them are doubly true for the online groups and
"awareness campaigns" of the 21<sup>st</sup> century:

[^4]:  Dating apps and websites might also count as exceptions in that the
       ultimate goal is to actually meet (*offline*) the person you have been
       flirting with.

> The bond between any two members [...] is less like the bond between two
> members of a gardening club or prayer group and more like the bond between
> two Yankees fans on opposite coasts (or perhaps two devoted L. L. Bean
> catalog users):  they share some of the same interests, but they are unaware
> of each other's existence.  Their ties are to common symbols, common
> leaders, and perhaps common ideals, but *not* to each other.[^5]

[^5]:  Robert D. Putnam, <u>Bowling Alone</u>

My purpose is not to condemn all social media.  There is nothing inherently
wrong with broadcasting one's identity to the world, and many people derive
enjoyment from it.  What I am cautioning against is thinking that our current
use of the internet and social media platforms is the ideal means of
'connecting' people.  Every medium of communication has a natural inertia, a
force which steers its interactions in a particular direction.  Social media
*can* be used for connecting people (and does in some cases), but it makes
broadcasting easier.

I try not to be the sort of person who offers criticism without solutions.  In
that spirit I'd like to attempt to end this piece with suggestions for
connecting people with the current technology as well as concrete examples of
what possible, connection-oriented platforms might look like.

The best way to successfully work against the inertia of the medium -- to
force ourselves to connect instead of broadcast -- is to use it to maintain
preexisting, offline social ties.  When connections are anchored in the real
world, social media can take on the same limited functionality as mail or
phone calls (e.g. sharing family photos on Facebook).  In this case the
platform becomes a tool rather than an end in itself.  Communicating through
private messages where possible is another strategy.  When communication is
either one-on-one or limited to a small group of friends, the interactions
become more sincere because there is less incentive to use the platform for
maintaining an artificial persona.

Of course, the previous suggestions require users to remain vigilant to swim
against the current of the medium.  If, on the other hand, we want to reverse
the inertia and make connection the default interaction, the goals for the
technology need to change.  Right now, social media platforms seem to be
focused on achieving the goal:  "How to more quickly and efficiently advertise
myself and be advertised to".  To truly connect with others, our technology
needs to achieve new goals.  Goals like:

* How to help someone else
* How to challenge my preconceived notions
* How to find common ground with a different person

Whereas broadcasting encourages one to reinforce or reinvent one's image,
connection (as promoted by the above goals) is to *better oneself for someone
else's benefit*.  If we wanted to encourage this kind of connection to be the
default on the internet, we would require new forms of "social media".  A
couple of examples:

**A website for connecting mentors with potential proteges.**  People who feel
they have a talent, skill, or other expertise to share can sign-up on a
website with their location.  Potential proteges can search the site by
location and/or expertise and can connect with mentors.  Monetary exchange
could be discouraged to emphasize connection based on shared passion.  I see
this sort of site encouraging connection across age boundaries where
younger users can learn from their elders and eventually become mentors
themselves to carry on the skills.

**A phone app for starting conversations with strangers.**  Users would
sign-up with a profile including their race, gender, education, and hobbies
along with a number of questions about values (political, spiritual, etc.) and
topics they like to talk about.  The phone app would notify a user when there
was someone else nearby that he might like to converse with and perhaps an
initial question to start the conversation.  The app would match users based
on *some* shared background (religion, political affiliation, etc.) but would
ensure that many interests did not overlap.  The goal of the platform would be
to start conversations between strangers who might walk away with a new
acquaintance or broadened worldview.[^6]

[^6]:  I acknowledge there are serious security concerns about such an app
       holding so much important information in a user database somewhere.

The above ideas exhaust my imagination but not, I'm sure, the possibilities of
the technology.  Perhaps it says something that the first solutions that come
to my mind force users to step away from their 'online presence'.  It's not
exactly an unpopular belief that the best way to connect is to step away from
our screens and take an active role in face-to-face interactions.  Maybe the
internet, and social media in particular, simply cannot be "fixed" to connect
people online.  I must remain optimistic that it can, however, because the
internet is not likely to go away.  If we are forced to live with the
technology, we should build it to suit our needs, not sacrifice part of our
humanity to adapt to it.

<br>

#### Footnotes
