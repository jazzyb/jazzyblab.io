---
layout: post
title:  "The End of Society"
author: jazzyb
---

> We are not used to a complicated civilization, we don't know how to behave
> when personal contact and eternal authority have disappeared.  There are no
> precedents to guide us, no wisdom that wasn't made for a simpler age.  We
> have changed our environment more quickly than we know how to change
> ourselves.
> 
> -- Walter Lippmann, <u>Drift and Mastery</u>

<br>

Between 1978 and 1995 Theodore J. Kaczynski delivered 16 bombs to people who
he deemed were advancing the progress of "modern industrial society" -- mostly
professors, corporate executives, and computer specialists.  The FBI dubbed
him the "Unabomber" after his initial targets: **UN**iversities and
**A**irlines.  His terrorism killed three, injured twenty-three, and ended
only when both The New York Times and The Washington Post agreed to publish
his 35,000-word manifesto, "Industrial Society and Its Future" (*ISAIF*).  In
his manifesto Kaczynski warns that technological society is "demeaning" to
people and is well on its way to enslaving or destroying the human race.  He
believes the only way to avoid this dystopic future and save humanity is an
international revolution against "the system" to destroy all modern technology
and throw the world back into pre-industrial society.  Kaczynski justifies his
violent means to popularizing these views:

> To make an impression on society with words therefore is almost impossible
> for most individuals and small groups. [...] In order to get our message
> before the public with some chance of making a lasting impression, we've had
> to kill people.[^1]

[^1]:  Theodore J. Kaczynski, "Industrial Society and Its Future", §96

What Kaczynski did not anticipate was that his terrorism, which led to the
media popularizing his manifesto, would ultimately prevent the majority of the
public from viewing the work as anything but mad ravings.  Various writers have
attempted to critique the manifesto but seem to stop short of addressing the
heart of his work, instead focusing on his obvious mental instability and
dearth of original ideas.  While it is true that Kaczynski is mentally ill and
many of his ideas have already been expressed in some form by better writers,
I believe he raises a number of cogent warnings.  In this post I'd like to
discuss one specific concern I share with him regarding the kind of society we
are developing and the role of technology in its development.

One of the fundamental ideas or "principles of history" that informs
Kaczynski's worldview is that "[p]eople do not consciously and rationally
choose the form of their society.  Societies develop through processes of
evolution that are not under human control."[^2]  I disagree that this is
necessarily a universal truth, but I do believe it is a fair assessment of
modern American society.  What Kaczynski means is that the agents of change
within society -- politicians, scientists, industry-leaders, and technologists
-- do not choose the actions they take in order to steer society towards a
long-term goal.  Rather, they are *at best* attempting to make small,
incremental changes for short-term benefits or *at worst* introducing changes
to society simply because they can.  (The latter is perhaps best exemplified
by the stereotypical Silicon Valley start-up.)  I don't mean to say that the
*individuals* involved lack positive long-term goals, but as a society we lack
a *unified* direction for civilization's progress.  That is, the "progress" --
if it can be called that -- of American society is best represented by a
random walk rather than an intentional progression towards a particular "end"
-- a set of ideals or goals to strive for.

[^2]:  Kaczynski, *ISAIF*, §106

Neil Postman lamented a similar problem he identified with modern schooling in
that it lacked a "god" or "narrative" to imbue the endeavor with purpose.  I
argue his criticism applies to society in general:

> For school to make sense, the young, their parents, and their teachers must
> have a god to serve, or, even better, several gods.  If they have none,
> school is pointless.
>
> [...]
>
> But it is the purpose of such [gods] to direct one's mind to an idea and,
> more to my point, to a story -- not any kind of story, but one that tells of
> origins and envisions a future, a story that constructs ideals, prescribes
> rules of conduct, provides a source of authority, and, above all, gives a
> sense of continuity and purpose.[^3]

[^3]:  Neil Postman, <u>The End of Education</u>

One may well wonder what danger there is in lacking such a goal-oriented
narrative.  Evolution is an unguided process; the free market is directed by
the distinct whims of individuals; and these processes provide adaptations and
goods, respectively, and do so at least sufficiently if not optimally.  Why
should social progress be any different?

Social progress's random walk has resulted in and will continue to result in
unplanned and arbitrary social environments.  Without directing the progress
of society towards identifiable goals of general benefit to the population, we
can't say that the society we've made is appropriate for human flourishing.
So when an individual's nature conflicts with society, we have no clear way to
determine whether the individual or the environment should be the one to
change.

Kaczynski proposes a hypothetical scenario that underscores this conflict[^4]:
Imagine that we introduced a change to society that caused psychological
distress for a small minority of the population.  Imagine further that these
distressed individuals could be given a drug that medicated them sufficiently
such that they could handle the new stressors and return to their normal way
of life.  Now that everyone in the environment is back to functioning
normally, more changes can be introduced which create even more stressors.
Now, individuals who before may have been borderline affected are pushed over
the edge into psychological distress and must be medicated to thrive in the
new environment.  This scenario repeats itself until every person in society
has been modified to accept an environment which is antagonistic to human
nature.  When there is a conflict between the individual and society, more
often than not the judgment is that the individual is the problem and must be
made to conform to society, not the other way around.

[^4]:  Kaczynski, *ISAIF*, §145

The above scenario, though extreme, isn't altogether fantastic.  The 1990's
saw a huge growth in the number of grade-schoolers taking medication to treat
ADHD so they could sit quietly and concentrate in the classroom.  Admittedly,
some children do have detrimental psychological problems which medication can
treat and improve, but how often do the people diagnosing this particular
condition consider that the children might be having perfectly understandable
reactions to a suffocating environment -- that the problem is the environment,
not the individual?  How many medications can we prescribe to someone to
combat the symptoms of their *other* medications before we start questioning
the arbitrary societal norms we've made?  From a 2016 article in Pacific
Standard Magazine:

> Melissa, a 28-year-old assistant to a financial advisor who took Ritalin in
> grade school, recalls coming home with her lunchbox full, day after day.
> "There were a few months when I actually stopped growing," she says. Sleep
> problems, not surprisingly, are also associated with stimulant use. "I had
> horrible insomnia," Brittany says. "When I was about 10 years old, they put
> me on Ambien to counteract the Adderall."[^5]

[^5]:  Madeleine Thomas, ["The Addicted Generation"](https://psmag.com/the-addicted-generation-92d7290bd171)

To be clear, I'm not denying that these treatments are effective in that they
allow individuals to sufficiently adapt to modern society; I'm worrying that
we might be treating the symptoms rather than the disease.  As long as the
progress of society remains arbitrary, we can't be entirely justified in
modifying the individual to suit the environment.

These problems will only become more serious as technology advances.  What if
genetic engineering becomes practical and widely available, and we discover
the genetic markers for ADHD?  If it's worth spending who-knows-how-many
dollars to treat people for ADHD throughout their whole lives, then surely
it's more cost-effective to modify that particular genetic sequence while the
embryo is still in the womb.  Such a procedure would undoubtedly *start out*
as optional, but how many parents wouldn't modify the genes of their baby if
they knew it would make it easier for them to get-by in society?  By simply
introducing the option, we could wind up making it necessary for parents to
modify their children's genes or risk them falling behind.  Perhaps ADHD is
such a mild issue that it would remain an optional decision, but what about
Asperger's or deafness?  If we know a child will be born with a more
significant behavioral or cognitive difference -- a difference that will make
it difficult for them to have a "normal life" in society -- will allowing that
child to be born with that "disorder" become equivalent to child abuse?

I'm sure these issues of medical ethics will come up in public debate long
before they are allowed to become commonplace.  They will be put to a vote,
and some arbitrary distinction will be agreed to (e.g. one will be allowed to
genetically remove physical disabilities but not cognitive or behavioral).  My
worries are two fold:  First, this technological progression is merely a more
thorough and efficient version of the same problem I described above with
respect to the conflict between individuals and their environments.  Second,
these legal decisions related to the technology will occur incrementally in a
series of compromises; thus, we risk being unable to initially anticipate the
world we are creating if we do not first have a firm vision of our ultimate
destination as a society.  If we do not decide what we want our society to
become beforehand, Kaczynski reminds us that we risk having arbitrary doctors
and lawyers "imposing their own values on the genetic constitution of the
population at large."[^6]

[^6]:  Kaczynski, *ISAIF*, §124

We need to change our perception of the relationship between society and the
individual, understanding that an individual's inability to adapt could be due
to a malfunction in the environment, not necessarily the individual.  We also
need to determine what sort of "end" or guiding principles we want our society
to have, and we need to do it now before chance decides our society's fate for
us.

<br>

#### Footnotes
