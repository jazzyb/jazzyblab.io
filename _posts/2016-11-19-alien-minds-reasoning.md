---
layout: post
title:  "Truly Alien Minds:  Reasoning"
author: jazzyb
permalink: /alien-minds-reasoning.html
---

#### Previous post in this series:  [Intelligence](/alien-minds-intelligence.html)

<br>

> Natural selection doesn't care about the truth or falsehood of your beliefs;
> it cares only about adaptive behavior.  Your beliefs may all be false,
> ridiculously false; if your behavior is adaptive, you will survive and
> reproduce.
> 
> -- Alvin Plantinga, "Evolution vs. Naturalism"

<br> Now that we've explored *what* 'optimization processes' are capable of --
namely achieving improbable goals -- let's shift our focus and examine *how*
an intelligent agent achieves such goals.  That is, when faced with a
decision, how would an alien mind go about deciding on a course of action, and
how might that process differ from our own?

Just so we don't have take into account "unthinkable thoughts", I will assume
for this post that our hypothetical alien visitors have an intelligence equal
in power to our own.  Instead of the unthinkable, this post will explore the
"unexpected" thoughts an alien decision-making process might generate.  Humans
are so limited to conversing with only a single kind of intelligent mind that
we don't think about how important modeling another's mental processes is to
communication, how important it is to *anticipate* reactions to our acts and
speech, and how being unable to do so can easily lead to confusion or
misunderstanding.  Since this failure to correctly anticipate reactions often
happens even among humans with the same decision-making hardware, we would
need to be extra careful when communicating with an extraterrestrial species.
Understanding how both rational and irrational decisions are made in humans
will give us some insight into how different this process might be for alien
minds.

Two fields of study, Decision Theory and Game Theory, attempt to understand
abstract decision-making processes given agents each with a set of values and
(possibly uncertain) beliefs about the world.  Both of these fields model
agents as rational decision-makers; a rational decision-maker is assumed to be
able to compute the optimal decision to take to achieve his goals with perfect
accuracy.  Due to my own ignorance I will avoid going into too much detail
about either of these subjects.  I only bring them up to point out that
formal, mathematical models of decision-making processes exist and to compare
them with the decision-making processes of real-world agents.  I'll use the
word 'reasoning' throughout to refer to both rational and irrational
decision-making processes.

Human 'reasoning' is made-up of both explicit and implicit (or intuitive)
reasoning systems.  The explicit reasoning system, which is evolutionarily
recent, is capable of modeling the normative theories of 'reasoning' given
by Decision and Game Theory.  We use explicit reasoning when we consciously
analyze and think through a problem in the same way that a scientist or
mathematician might perform their work.  Implicit reasoning, which is a much
older system, might be considered "common sense"; it is the reasoning we tend
to use most of the time in everyday situations like when we're deciding what
to eat or how to respond to a stranger.  None of the formal models apply as a
descriptive theory of implicit reasoning because human intuition was never
"designed" with finding the optimal solution to problems, only one good enough
to pass on an individual's genes.

In the [previous post](/alien-minds-intelligence.html) I briefly mentioned
evolution's role in human intelligence.  Evolution, like intelligence, is also
a kind of 'optimization process' but much less powerful than human
intelligence.  Whereas human intelligence can invent relatively novel
solutions, the search algorithm of natural selection is limited to more local
maxima -- new or better functionality is typically built piecemeal upon the
structures that already happen to be present.[^1]  Well-known examples include
the blind spot of the eye[^2] and the S-curve of the spine[^3], but it's
important to note that our own 'reasoning' (and in particular our implicit
reasoning system) is the product of this same piecemeal process.

[^1]:  If you are interested in a thorough but accessible introduction to
       the theory of evolution via natural selection, I recommend the book
       <u>What Evolution Is</u> by Ernst Mayr.

[^2]:  "Right smack in the middle of the retina, there is a structure called
       the optic disc where the axons of the millions of photoreceptor cells
       all converge to form the optic nerve. The disc is located on the surface
       of the retina, occupying a small circular spot in which no photoreceptor
       cells can fit."
       ([source](https://thehumanevolutionblog.com/2015/01/12/the-poor-design-of-the-human-eye/))

[^3]:  "When humans stood upright, they took a spine that had evolved to be
       stiff for climbing and moving in trees and rotated it 90 degrees, so it
       was vertical—a task Latimer compared to stacking 26 cups and saucers on
       top of each other (vertebrae and discs) and then, balancing a head on
       top. But so as not to obstruct the birth canal and to get the torso
       balanced above our feet, the spine has to curve inwards (lordosis),
       creating the hollow of our backs."
       ([source](http://www.sciencemag.org/news/2013/02/human-evolution-gain-came-pain))

As a result of the evolutionary process, implicit reasoning differs from the
normative theory in two key ways:  First, potential losses tend to be weighed
more heavily than potential gains when evaluating the outcome of a decision --
i.e., we tend to be more risk-averse than the ideal rational decision-maker.
Second, decisions aren't based on a single, uniform algorithm but rather a
collection of heuristics of varying reliability.  The context in which a
decision is made -- environmental surroundings, emotional states, etc. -- will
determine which heuristic is used.  Because rational decision-making is so
computationally expensive, the heuristics involved in implicit reasoning take
time- and energy-conserving shortcuts that often result in non-optimal, but
"good-enough", decisions.

However, sometimes our implicit reasoning can be fooled into making not just
sub-optimal but blatantly wrong decisions.  When our 'reasoning' heuristics
introduce such an error, we call it "cognitive bias".  For example, say you
are presented with the following scenario:

> Consider a regular six-sided die with four green faces and two red faces.
> The die will be rolled 20 times and the sequence of greens (G) and reds (R)
> will be recorded. You are asked to select one sequence, from a set of three,
> and you will win $25 if the sequence you chose appears on successive rolls
> of the die. Please check the sequence of greens and reds on which you prefer
> to bet.
> 
> 1. `RGRRR`
> 2. `GRGRRR`
> 3. `GRRRRR`

When the psychologists Amos Tversky and Daniel Kahneman performed this
experiment, 65% of participants chose `GRGRRR` as more likely to appear than
`RGRRR` even though `RGRRR` is a subset of `GRGRRR` and is, therefore,
obviously more likely to occur.  Tversky and Kahneman argue:

> These results suggest that subjects coded each sequence in terms of the
> proportion of Gs and Rs and ranked the sequences by the discrepancy between
> the proportions in the two sequences (1/5 and 1/3) and the expected value of
> 2/3.[^4]

[^4]:  Amos Tversky and Daniel Kahneman, "Extensional Versus Intuitive Reasoning:  The Conjunction Fallacy in Probability Judgment"

The heuristic involved in this situation moved most of the participants to
make a decision based on which sequence seemed like the best representation of
the die faces.  This quick heuristic would produce correct results in many
cases; I assume it would have moved participants to choose the more
probable `GRGGR` had that sequence been among the choices; but this example
demonstrates how our 'reasoning' shortcuts can fail in irrational ways.

Returning to our hypothetical alien species, we can assume that their
'reasoning' is also the product of an imperfect evolutionary process.  If
the aliens have an explicit reasoning process, we should expect it to be
relatively similar to ours since it is based on making accurate predictions
about the universe and we both inhabit the same universe with the same rules.
Implicit reasoning, on the other hand, could result in odd behavior.  There is
no guarantee that their evolutionary path selected for the same set of
heuristics with the same biases.  Similar to our own implicit reasoning, we
would expect their heuristics to make the right decisions most of the time,
but they could fail in unpredictable and arbitrary ways.  This could be
especially true when they find themselves in an environment, like Earth, that
differs greatly from that of their evolutionary past.

For example, say that the aliens' native planet has a genus of deadly
predators that look like a thick, brown branch with five thin tendrils on
their head.  They might have evolved a fear, aversion, or aggression response
that kicks-in uncontrollably whenever they see one of these creatures.  Should
our hypothetical aliens have appendages that would prompt one to attempt a
handshake, they may respond with surprising and unprovoked aggression.

Another example:  Say that the aliens have evolved a very quick heuristic for
counting sets of similar objects.  They can recognize a group of five similar
items quickly and, using this, can estimate a number of similar objects to the
nearest five; e.g. they would estimate 22 similar objects to be 20 or 24 to be
25 almost instantaneously.  However, due to the way that the heuristic works,
the aliens *actually perceive* the wrong number of objects:  When looking at
22 people, for example, two people will seem to disappear to them, or when
looking at 24 people, a "phantom" person will appear beside the others that
doesn't actually exist when they count them individually -- similar to the way
a white triangle "just appears" to humans when glancing at the following
picture but disappears under scrutiny:

{:refdef: style="text-align: center;"}
![Kanizsa Triangle]({{ site.base }}/images/Kanizsa_triangle.svg)
{: refdef}

{:refdef: style="text-align: center;font-size: 12px;"}
*[Image](https://upload.wikimedia.org/wikipedia/commons/5/55/Kanizsa_triangle.svg) by [Fibonacci](https://commons.wikimedia.org/wiki/User:Fibonacci) / [CC BY-SA](https://creativecommons.org/licenses/by-sa/3.0/deed.en)*
{: refdef}

Imagine the confusion if an alien visitor kept accidentally addressing the
non-existent fifth crew member of a four-person team of human communicators or
if it seemed unable to keep track of every member of a six-person team at the
same time.  How long would we need to engage in communication and with how
many different alien individuals before we could even distinguish between
sanity and insanity in an alien species?

We are able to understand and predict human cognitive biases (and distinguish
them from the ravings of madmen for that matter) because all humans share
the same reasoning heuristics and can empathize with the choices other humans
make.  Even if you got the correct answer in the dice example above, you
likely understood *why* someone would pick the wrong answer.  By comparison an
alien mind might make a perfectly reasonable decision from our perspective in
one situation and behave unpredictably given the same choice in a different
situation.  To predict the behavior of a creature who doesn't share our
evolutionary past, we would need to memorize a jumbled hierarchy of heuristics
which are chosen based on seemingly arbitrary environmental contexts.
Ironically, we would need to use our explicit reasoning system in order to
empathize with or think like an alien mind.  The same would be true from the
aliens' perspective as well.  Our intuitive decisions, which make so much
sense to us, would possibly catch our visitors by surprise and in some cases
may even be indistinguishable from madness.

For these reasons it might make sense, if we ever do make contact with an
alien race, to limit initial communication to asynchronous channels as much as
possible, like text.  Text would limit the exposure to sensory input that
might instinctively lead to misunderstandings by both humans and aliens.  Any
asynchronous means of communication would give both parties enough time for
responses that rely on rational, explicit reasoning.  Even first contact
between different humans can be an error-prone and possibly dangerous affair
due to cultural differences.  We must be exponentially more cautious when the
recipients of our messages share nothing of our instincts or intuitions.

<br>

#### Next post in this series:  **Understanding**

<br>

#### Footnotes
