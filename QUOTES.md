> This government had been imposed, by a few property holders, upon a people
> too poor, too scattered, and many of them too ignorant, to resist.  It had
> been carried on, for some seventy years, by a mere cabal of irresponsible
> men, called lawmakers.  In this cabal, the several local bands of robbers --
> the slaveholders of the South, the iron monopolists, the woollen
> monopolists, and the money monopolists, of the North -- were represented.
> The whole purpose of its laws was to rob and enslave the many -- both North
> and South -- for the benefit of the few.  But these robbers and tyrants
> quarreled -- as lesser bands of robbers have done -- over the division of
> their spoils.  And hence the war.  No principle as justice to anybody --
> black or white -- was the ruling motive on either side.
> 
> -- Lysander Spooner, "Letter to Grover Cleveland"

tags: civil war
---

> See, I'm a philosopher.  And I'm not going to argue very much because if you
> don't argue with me, I don't know what I think.  So if we argue I say,
> "Thank you."  Because going through the courtesy of your taking a different
> view, I understand what I mean.  So I can't get rid of you.
> 
> -- Alan Watts, ["A Game That's Worth the Candle"](https://www.youtube.com/watch?v=8HAUEJhzsfE&t=5m27s)

tags: philosophy, communication, debate
---

> You evidently believe that a certain paper, called the constitution, which
> nobody ever signed, which few persons ever read, which the great body of the
> people never saw, and as to the meaning of which no two persons were ever
> agreed, is the supreme law of this land, anything in the law of nature --
> anything in the natural, inherent, inalienable, *individual* rights of fifty
> millions of people -- to the contrary notwithstanding.
>
> Did folly, falsehood, absurdity, assumption, or criminality ever reach a
> higher point than that?
> 
> -- Lysander Spooner, "Letter to Grover Cleveland"
---

> Take any two persons, for example, who have witnessed some tragedy, a big
> fire, let us say, at the same time and place.  Each will tell the story in a
> different manner, each will be original in his way of relating it and in the
> impression he will produce, because of his naturally different psychology.
> But talk to the same two persons on some fundamental social matter, about
> life and government, for instance, and immediately you hear expressed an
> exactly similar attitude, the accepted view, the dominant mentality.
> 
> -- Alexander Berkman, <u>ABC of Anarchism</u>
---

> If we have the truth, it cannot be harmed by investigation.  If we have not
> the truth, it ought to be harmed.
> 
> -- J. Reuben Clark, as quoted by D. Michael Quinn in <u>J. Reuben Clark: The Church Years</u>
---

> If we find an irrational area in an otherwise rational person we can be sure
> that it hides something important.  The fight that is often put up against
> the quality of irrationality is usually in reality a fight against having
> its background uncovered.
> 
> -- Karen Horney, <u>Self-Analysis</u>

tags: psychology, rationality
---

> Let us take a look, for example, at the conduct of our own country in its
> foreign affairs. By and large we find, if we listen to the statements of our
> leaders during the past several years, and read their documents, that our
> diplomacy is always based upon high moral purposes; that it is always
> consistent with the policies we have followed previously; that it involves
> no selfish desires; and that it has never been mistaken in its judgments and
> choices.  I think perhaps you will agree with me that if we heard an
> individual speaking these terms we would recognize at once that this must be
> a façade, that such statements could not possibly represent the real process
> going on within himself.
> 
> -- Carl R. Rogers, "To Be the Self Which One Truly Is"

tags: politics, self-deception
---

> I have taken a long walk through the Neck, as they call it, a fine tract of
> land in a general field.  Corn, rye, grass, interspersed in great perfection
> this fine season.  I wander alone and ponder.  I muse, I mope, I ruminate.
> I am often in reveries and brown studies.  The objects before me are too
> great and multifarious for my comprehension.  We have not men fit for the
> times.  We are deficient in genius, in eduction, in travel, in fortune, in
> every thing.  I feel unutterable anxiety.
> 
> -- John Adams, 25 June 1774
> (*journal entry during the First Continental Congress*)

tags: despair, hindsight, hope
---

> Nothing pushes back the progress of knowledge like a bad work by a famous
> author, because before instructing, one must begin by correcting the
> mistakes.
> 
> -- Montesquieu, <u>The Spirit of the Laws</u>

tags: scholarship, books
---

> The oracles have ceased because the places where they spoke have been
> destroyed.
> 
> -- Plutarch, <u>Moralia, De defectu oracularum</u>
---

> Sir William Petty has assumed in his calculations that a man in England is
> worth what he would be sold for in Algiers.  This can be good only for
> England:  there are countries in which a man is worth nothing; there are
> some in which he is worth less than nothing.
> 
> -- Montesquieu, <u>The Spirit of the Laws</u>
---

> Above all, don't lie to yourself.  The man who lies to himself and listens
> to his own lie comes to a point that he cannot distinguish the truth within
> him, or around him, and so loses all respect for himself and for others.
> And having no respect he ceases to love.
> 
> -- Fyodor Dostoyevsky, <u>The Brothers Karamazov</u>

tags: honesty
---

> It is, however, more advantageous and more to the point to subject to the
> most rigorous scrutiny one's own moods and their changing influence on one's
> personality.  To know where the other person makes a mistake is of little
> value.  It only becomes interesting when you know where *you* make the
> mistake, for then you can do something about it.
> 
> -- Carl Jung, <u>Aion</u>

tags: self-improvement, honesty
---

> In order to form a moderate government, one must combine powers, regulate
> them, temper them, make them act; one must give one power a ballast, so to
> speak, to put it in a position to resist another; this is a masterpiece of
> legislation that chance rarely produces and prudence is rarely allowed to
> produce.
> 
> -- Montesquieu, <u>The Spirit of the Laws</u>

tags: law, good government
---

> [N]o one is a tyrant there without at the same time being a slave.  Extreme
> obedience assumes ignorance in the one who obeys; it assumes ignorance even
> in the one who commands; he does not have to deliberate, to doubt, or to
> reason; he has only to want.
> 
> -- Montesquieu, <u>The Spirit of the Laws</u>

tags: tyranny, slavery
---

> If this work meets with success, I shall owe much of it to the majesty of my
> subject; still, I do not believe that I have totally lacked genius.  When I
> have seen what so many great men in France, England, and Germany have
> written before me, I have been filled with wonder, but I have not lost
> courage.  "And I too am a painter," have I said with Correggio.
> 
> -- Montesquieu, <u>The Spirit of the Laws</u>

tags: perseverance, humility
---

> The question of what kind of city we want cannot be divorced from the
> question of what kind of people we want to be, what kinds of social
> relations we seek, what relations to nature we cherish, what style of life
> we desire, or what aesthetic values we hold.
> 
> -- David Harvey, <u>Rebel Cities: From the Right to the City to the Urban Revolution</u>

tags: choice, society
---

> It is indeed true that "It is not from the benevolence of the butcher, the
> brewer or the baker, that we expect our dinner, but from their regard to
> their own interest" (Adam Smith, *The Wealth of Nations* I, ii).  And just
> as butcher, brewer, and baker generally act with regard to their own
> interest, so too do their customers.  But if on entering the butcher's shop
> as a habitual customer I find him collapsing from a heart attack, and I
> merely remark 'Ah!  Not in a position to sell me my meat today, I see,' and
> proceed immediately to his competitor's shop to complete my purchase, I will
> have obviously and grossly damaged my *whole* relationship to him, including
> my economic relationship, although I will have done nothing contrary to the
> norms of the market.  [...]  Market relationships can only be sustained by
> being embedded in certain types of local nonmarket relationship,
> relationships of uncalculated giving and receiving, if they are to
> contribute to overall flourishing, rather than, as they so often in fact do,
> undermine and corrupt communal ties.
> 
> -- Alasdair MacIntyre, <u>Dependent Rational Animals</u>

tags: altruism, capitalism, community, economics, ethics
---

> For when I consider how short were the Lawes of antient times; and how they
> grew by degrees still longer; me thinks I see a contention between the
> Penners, and Pleaders of the Law; the former seeking to circumscribe the
> later; and the later to evade their circumscriptions; and that the Pleaders
> have got the Victory.
> 
> -- Thomas Hobbes, <u>Leviathan</u>

tags: legal ambiguity, law, legislation, government
---

> The future historian of "thought and expression" in the twentieth century
> will no doubt record with some amusement the ingenious trick, which some of
> the philosophical controversialists of the first quarter century had, of
> labelling their opponents' views "fallacies".
> 
> -- W. K. Frankena, "The Naturalistic Fallacy"

tags: philosophy, logical fallacy
---

> Consider the following possibility:  that what we are oppressed by is not
> power, but impotence; that the one key reason why the presidents of large
> corporations do not, as some radical critics believe, control the United
> States is that they do not even succeed in controlling their own
> corporations; that all too often, when imputed organizational skill and
> power are deployed and the desired effect follows, all that we have
> witnessed is the same kind of sequence as that to be observed when a
> clergyman is fortunate enough to pray for rain just before the unpredicted
> end of a drought...
> 
> -- Alasdair MacIntyre, <u>After Virtue</u>

tags: government, bureaucracy, coincidence, corporate control
---

> Twentieth century moral philosophers have sometimes appealed to their and
> our intuitions; but one of the things that we ought to have learned from the
> history of moral philosophy is that the introduction of the word 'intuition'
> by a moral philosopher is always a signal that something has gone badly
> wrong with an argument.
> 
> -- Alasdair MacIntyre, <u>After Virtue</u>

tags: ethics, morals
---

> For as a man that is born blind, hearing men talk of warming themselves by
> the fire, and being brought to warm himself by the same, may easily
> conceive, and assure himselfe, there is somewhat there, which men call
> *Fire*, and is the cause of the heat he feeles; but cannot imagine what it
> is like; nor have an Idea of it in his mind, such as they have that see it:
> so also, by the visible things of this world, and their admirable order, a
> man may conceive there is a cause of them, which men call God; and yet not
> have an Idea, or Image of him in his mind.
> 
> -- Thomas Hobbes, <u>Leviathan</u>

tags: religion, theology, apologetics, design
---

> For words are wise mens counters, they do but reckon by them: but they are
> the mony of fooles, that value them by the authority of an *Aristotle*, a
> *Cicero*, or a *Thomas*, or any other Doctor whatsoever, if but a man.
> 
> -- Thomas Hobbes, <u>Leviathan</u>

tags: reason, justification, authority
---

> The Great Society created by steam and electricity may be a society, but it
> is no community.  The invasion of the community by the new and relatively
> impersonal and mechanical modes of combined human behavior is the
> outstanding fact of modern life.
> 
> -- John Dewey, <u>The Public and its Problems</u>

tags: democracy, technology, modernity
---

> If you’re interested in being on the right side of disputes, you will refute
> your opponents' arguments.  But if you’re interested in producing truth, you
> will fix your opponents' arguments for them.  To win, you must fight not
> only the creature you encounter; you must fight the most horrible thing that
> can be constructed from its corpse.
> 
> -- Steven Kaas, attributed via [Rationality Quotes 13](http://lesswrong.com/lw/tm/rationality_quotes_13/)

tags: honesty, reason, debate
---

> The mass media are mostly under the control of large organizations that are
> integrated into the system.  [...]  To make an impression on society with
> words therefore is almost impossible for most individuals and small groups.
> Take us (FC) for example. [...] In order to get our message before the
> public with some chance of making a lasting impression, we've had to kill
> people.
> 
> -- Theodore J. Kaczynski, "Industrial Society and Its Future"

tags: media, freedom of the press
---

> Suppose that a group of democratic republics form a consortium to develop
> AI [Artificial Intelligence], and there's a lot of politicking during the
> process -- some interest groups have unusually large influence, others get
> shafted -- in other words, the result looks just like the products of modern
> democracies.  Alternatively, suppose a group of rebel nerds develops an AI
> in their basement, and instructs the AI to poll everyone in the world --
> dropping cellphones to anyone who doesn't have them -- and do whatever the
> majority says. Which of these do you think is more "democratic," and would
> you feel safe with either?
> 
> -- Eliezer Yudkowsky, ["Applause Lights"](http://lesswrong.com/lw/jb/applause_lights/)

tags: democracy, technology
---

> Modern society is in certain respects extremely permissive.  In matters that
> are irrelevant to the functioning of the system we can generally do what we
> please.  We can believe in any religion we like (as long as it does not
> encourage behavior that is dangerous to the system).  We can go to bed with
> anyone we like (as long as we practice "safe sex").  We can do anything we
> like as long as it is UNIMPORTANT.  But in all IMPORTANT matters the system
> tends increasingly to regulate our behavior.
> 
> -- Theodore J. Kaczynski, "Industrial Society and Its Future"

tags: autonomy, freedom, individuality, conformity
---

> A believing Communist sees the wisdom of Marx in every hamburger bought at
> McDonald's; in every promotion they're denied that would have gone to them
> in a true worker's paradise; in every election that doesn't go to their
> taste; in every newspaper article "slanted in the wrong direction." Every
> time they use the Great Idea to interpret another event, the Great Idea is
> confirmed all the more. It feels better -- positive reinforcement -- and of
> course, when something feels good, that, alas, makes us *want* to believe it
> all the more.
> 
> -- Eliezer Yudkowsky, ["Affective Death Spirals"](http://lesswrong.com/lw/lm/affective_death_spirals/)

tags: communism, perception, confirmation
---

> Penetrating computer security is really quite dull; I can see how it might
> attract those who can't resist a challenge to their cleverness, but it's not
> intellectually aesthetic at all.  It's no different than tugging on the
> doors of a locked house until you find an improperly installed lock.  A
> useful activity, but hardly interesting.
> 
> -- Ted Chiang, "Understand"

tags: hacking
---

> And we must look into ourselves, into the depth of our souls.  We must
> become something we have never been and for which our education and
> experience and environment have ill-prepared us.  We must become bigger than
> we have been:  more courageous, greater in spirit, larger in outlook.  We
> must become members of a new race, overcoming petty prejudice, owing our
> ultimate allegiance not to nations but to our fellow men within the human
> community.
> 
> -- Haile Selassie, "Address to the United Nations", 4 October 1963

tags: courage, humanity
---

> Political communications specialist Roderick Hart argues that television as
> a medium creates a false sense of companionship, making people *feel*
> intimate, informed, clever, busy, and important.  The result is a kind of
> "remote control politics", in which we as viewers *feel* engaged with our
> community without the effort of actually *being* engaged.  Like junk food,
> TV, especially TV entertainment, satisfies craving without real nourishment.
> 
> -- Robert D. Putnam, <u>Bowling Alone</u>

tags: television, technology, information
---

> While I like the sentiment here, I think the danger is that engineers might
> come to the mistaken conclusion that making pizzas is the primary limiting
> reagent to running a successful pizzeria. Running a successful pizzeria is
> more about schlepping to local hotels and leaving them 50 copies of your
> menu to put at the front desk, hiring drivers who will both deliver pizzas
> in a timely fashion and not embezzle your (razor-thin) profits while also
> costing next-to-nothing to employ, maintaining a kitchen in sufficient order
> to pass your local health inspector's annual visit (and dealing with 47
> different pieces of paper related to that), being able to juggle priorities
> like "Do I take out a bank loan to build a new brick-oven, which will make
> the pizza taste better, in the knowledge that this will commit $3,000 of my
> cash flow every month for the next 3 years, or do I hire an extra cook?",
> sourcing ingredients such that they're available in quantity and quality
> every day for a fairly consistent price, setting prices such that they're
> locally competitive for your chosen clientele but generate a healthy gross
> margin for the business, understanding why a healthy gross margin really
> doesn't imply a healthy net margin and that the rent still needs to get
> paid, keeping good-enough records such that you know whether your business
> is dying before you can't make payroll and such that you can provide a
> reasonably accurate picture of accounts for the taxation authorities every
> year, balancing 50% off medium pizza promotions with the desire to not
> cannibalize the business of your regulars, etc etc, and by the way tomato
> sauce should be tangy but not sour and cheese should melt with just the
> faintest whisp [sic] of a crust on it.
>
> Do you want to write software for a living? Google is hiring. Do you want to
> run a software business? Godspeed. Software is now 10% of your working life.
> 
> -- Patrick McKenzie, [a comment on Hacker News](https://news.ycombinator.com/item?id=4060229)

tags: software, engineering, business
